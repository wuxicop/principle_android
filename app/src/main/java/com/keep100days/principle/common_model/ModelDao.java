package com.keep100days.principle.common_model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ModelDao {

    @Query("select * FROM common_model_table where type =:type order by model_index asc")
    List<CommonModel> getAll(long type);

    @Query("select * FROM common_model_table where type =:type order by update_time desc")
    List<CommonModel> getAllOrderByTime(long type);

    @Query("select * FROM common_model_table where type =:type and content = :content")
    List<CommonModel> query(String content, long type);

    @Query("select * FROM common_model_table where type =:type and common_integer = :directId")
    List<CommonModel> query(long directId, long type);

    @Query("select * FROM common_model_table where id =:id")
    List<CommonModel> query(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertRecord(CommonModel entity);

    @Query("delete FROM common_model_table where type =:type")
    int deleteByType(long type);

    @Delete()
    int deleteRecord(CommonModel entity);

    @Query("update common_model_table set content=:content where id =:id")
    int updateRecordContentById(long id, String content);

    @Query("update common_model_table set model_index=:index where id =:id")
    void updateRecordIndexById(long id, long index);

    @Update()
    void update(CommonModel model);
}
