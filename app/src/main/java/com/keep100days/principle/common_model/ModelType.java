package com.keep100days.principle.common_model;

/**
 * @author a
 */
public class ModelType {

    /**
     * 事件池类型
     */
    public static final long TYPE_EVENT_POOL = 0;

    /**
     * 笔记备注类型（备注列表选项功能）
     */
    public static final long TYPE_MARK = 1;

    /**
     * 文件夹类型（笔记）
     */
    public static final long TYPE_NOTE_DIRECT = 2;

    /**
     * 文件夹item类型（笔记）
     */
    public static final long TYPE_NOTE_DIRECT_ITEM = 3;

}
