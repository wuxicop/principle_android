package com.keep100days.principle.common_model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author a
 */

@Entity(tableName = "common_model_table")
public class CommonModel {

    public CommonModel(String content, long type) {
        setType(type);
        setContent(content);
        long time = System.currentTimeMillis();
        setCreateTime(time);
        setUpdateTime(time);
    }

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "create_time")
    private long createTime;

    @ColumnInfo(name = "update_time")
    private long updateTime;

    private long type;

    @ColumnInfo(name = "model_index")
    private long index;

    private String content;

    @ColumnInfo(name = "common_string")
    private String commonString;

    @ColumnInfo(name = "common_integer")
    private int commonInteger;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCommonString() {
        return commonString;
    }

    public void setCommonString(String commonString) {
        this.commonString = commonString;
    }

    public int getCommonInteger() {
        return commonInteger;
    }

    public void setCommonInteger(int commonInteger) {
        this.commonInteger = commonInteger;
    }

    public static String getCreateSQL() {
        return "CREATE TABLE IF NOT EXISTS common_model_table ( "
                + "id INTEGER PRIMARY KEY NOT NULL,"
                + "create_time INTEGER NOT NULL,"
                + "update_time INTEGER NOT NULL,"
                + "type INTEGER NOT NULL,"
                + "model_index INTEGER NOT NULL,"
                + "content TEXT"
                + " );";
    }

    public static String[] getAlterSQL() {
        return new String[]{
                "ALTER TABLE common_model_table ADD COLUMN common_string TEXT",
                "ALTER TABLE common_model_table ADD COLUMN common_integer INTEGER DEFAULT 0 NOT NULL"
        };
    }
}
