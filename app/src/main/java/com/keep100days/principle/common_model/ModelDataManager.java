package com.keep100days.principle.common_model;

import android.content.Context;

import com.keep100days.principle.db.AppDatabaseHelper;

import java.util.List;

/**
 * @author a
 */
public class ModelDataManager {

    public static List<CommonModel> getAll(Context context, long type) {
        try {
            return AppDatabaseHelper.getInstance(context).modelDao().getAll(type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<CommonModel> getAllOrderByTime(Context context, long type) {
        try {
            return AppDatabaseHelper.getInstance(context).modelDao().getAllOrderByTime(type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<CommonModel> query(Context context, String content, long type) {
        try {
            return AppDatabaseHelper.getInstance(context).modelDao().query(content, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<CommonModel> query(Context context, long directId, long type) {
        try {
            return AppDatabaseHelper.getInstance(context).modelDao().query(directId, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<CommonModel> query(Context context, long id) {
        try {
            return AppDatabaseHelper.getInstance(context).modelDao().query(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void insert(Context context, CommonModel model) {
        try {
            long id = AppDatabaseHelper.getInstance(context).modelDao().insertRecord(model);
            updateIndexById(context, id, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void delete(Context context, CommonModel model) {
        try {
            AppDatabaseHelper.getInstance(context).modelDao().deleteRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteByType(Context context, long type) {
        try {
            AppDatabaseHelper.getInstance(context).modelDao().deleteByType(type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateIndexById(Context context, long id, long index) {
        try {
            AppDatabaseHelper.getInstance(context).modelDao().updateRecordIndexById(id, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, long id, String content) {
        try {
            AppDatabaseHelper.getInstance(context).modelDao().updateRecordContentById(id, content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, CommonModel model) {
        try {
            AppDatabaseHelper.getInstance(context).modelDao().update(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void swapIndex(Context context, CommonModel left, CommonModel right) {
        long leftIndex = left.getIndex();

        updateIndexById(context, left.getId(), right.getIndex());
        updateIndexById(context, right.getId(), leftIndex);
    }

}
