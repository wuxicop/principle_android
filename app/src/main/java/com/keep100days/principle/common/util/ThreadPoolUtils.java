package com.keep100days.principle.common.util;

import android.util.Log;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolUtils {

    private static final String TAG = "ThreadPoolUtils";

    private ThreadPoolUtils() {
    }

    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();

    private static final int CORE_POOL_SIZE = Math.max(2, Math.min(CPU_COUNT - 1, 4));

    private static final int MAX_POOL_SIZE = CPU_COUNT * 2 + 1;

    private static int KEEP_ALIVE_TIME = 30;

    private static final BlockingQueue<Runnable> WORK_QUEUE =
            new LinkedBlockingQueue<Runnable>(128);

    private static ThreadFactory threadFactory = new ThreadFactory() {
        private final AtomicInteger integer = new AtomicInteger();

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "myThreadPool thread:" + integer.getAndIncrement());
        }
    };

    private static int count = 0;

    private static RejectedExecutionHandler handler = new RejectedExecutionHandler() {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            if (count == 0) {
                Log.i(TAG, "blocking task size too long.");
            }
            count++;
            if (count == 10) {
                count = 0;
            }
        }

    };


    private static ThreadPoolExecutor threadPool;

    static {
        threadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, WORK_QUEUE, threadFactory, handler);
        threadPool.allowCoreThreadTimeOut(true);
    }


    public static void execute(Runnable runnable) {
        if (runnable == null) {
            return;
        }
        threadPool.execute(runnable);
    }

}

