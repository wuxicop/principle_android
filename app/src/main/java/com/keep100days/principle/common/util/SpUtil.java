package com.keep100days.principle.common.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.keep100days.principle.common.ui.DYEditActivity;

public class SpUtil {

    private static final String SP_NAME = "sp_priciple";
    private static final String KEY_ANNUAL_PLAN = "KEY_ANNUAL_PLAN";
    private static final String KEY_LAST_DY_TIME = "KEY_LAST_DY_TIME";
    private static final String KEY_LAST_DY_INDEX = "KEY_LAST_DY_INDEX";
    private static final String KEY_ONLY_SHOW_NOT_DONE_TODO = "KEY_ONLY_SHOW_NOT_DONE_TODO";
    private static final String KEY_ONLY_SHOW_TODAY_TODO = "KEY_ONLY_SHOW_TODAY_TODO";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
    }

    private static void saveString(Context context, String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    private static String getString(Context context, String key) {
        return getSharedPreferences(context).getString(key, "");
    }

    private static void saveLong(Context context, String key, long value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putLong(key, value);
        editor.apply();
    }

    private static long getLong(Context context, String key) {
        return getSharedPreferences(context).getLong(key, 0);
    }

    private static boolean getBoolean(Context context, String key) {
        return getSharedPreferences(context).getBoolean(key, false);
    }

    private static void saveBoolean(Context context, String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static String getAnnualPlan(Context context) {
        return getString(context, KEY_ANNUAL_PLAN);
    }

    public static void saveAnnualPlan(Context context, String content) {
        saveString(context, KEY_ANNUAL_PLAN, content);
    }

    public static boolean isOnlyShowTodayTodo(Context context) {
        return getBoolean(context, KEY_ONLY_SHOW_TODAY_TODO);
    }

    public static void saveIsOnlyShowTodayTodo(Context context, boolean onlyToday) {
        saveBoolean(context, KEY_ONLY_SHOW_TODAY_TODO, onlyToday);
    }

    public static boolean isOnlyShowNotDoneTodo(Context context) {
        return getBoolean(context, KEY_ONLY_SHOW_NOT_DONE_TODO);
    }

    public static void saveIsOnlyShowNotDoneTodo(Context context, boolean onlyNotDone) {
        saveBoolean(context, KEY_ONLY_SHOW_NOT_DONE_TODO, onlyNotDone);
    }

    public static long getLastDYEditTime(Context context) {
        return getLong(context, KEY_LAST_DY_TIME);
    }

    public static void saveLastDYEditTime(Context context, long content) {
        saveLong(context, KEY_LAST_DY_TIME, content);
    }

    public static void saveLastDYEditIndex(Context context, long content) {
        saveLong(context, KEY_LAST_DY_INDEX, content);
    }

    public static long getLastDYEditIndex(Context context) {
        return getLong(context, KEY_LAST_DY_INDEX);
    }
}
