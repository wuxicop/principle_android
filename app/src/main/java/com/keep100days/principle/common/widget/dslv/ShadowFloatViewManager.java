package com.keep100days.principle.common.widget.dslv;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.keep100days.principle.R;


/**
 * NotesFloatViewManager
 */
public class ShadowFloatViewManager implements DragSortListView.FloatViewManager {


    private Bitmap mFloatBitmap;

    private ImageView mImageView;

    private int mFloatBGColor = Color.BLACK;

    private ListView mListView;

    /**
     * A listener that receives a callback after floating View create
     */
    private OnCreateFloatViewListener mOnCreateFloatViewListener;
    private int mPaddingTop;
    private int mpaddingBottom;

    public ShadowFloatViewManager(ListView lv) {
        mListView = lv;
    }

    public void setBackgroundColor(int color) {
        mFloatBGColor = color;
    }

    /**
     * This simple implementation creates a Bitmap copy of the
     * list item currently shown at ListView <code>position</code>.
     */
    @Override
    public View onCreateFloatView(int position) {
        // Guaranteed that this will not be null? I think so. Nope, got
        // a NullPointerException once...
        View v = mListView.getChildAt(position + mListView.getHeaderViewsCount() - mListView.getFirstVisiblePosition());

        if (v == null) {
            return null;
        }

        if (mOnCreateFloatViewListener != null) {
            mOnCreateFloatViewListener.onCreateFloatView();
        }

        v.setPressed(false);

        // Create a copy of the drawing cache so that it does not get
        // recycled by the framework when the list tries to clean up memory
        //v.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        v.setDrawingCacheEnabled(true);

        Drawable drawableTop = mListView.getContext().getResources()
                .getDrawable(R.mipmap.drag_item_shadow_top);

        Drawable drawableBottom = mListView.getContext().getResources()
                .getDrawable(R.mipmap.drag_item_shadow_bottom);

        Bitmap lineTop = ((BitmapDrawable) drawableTop).getBitmap();
        Bitmap lineBottom = ((BitmapDrawable) drawableBottom).getBitmap();

        Bitmap body = v.getDrawingCache();

        int width = body.getWidth();

        mPaddingTop = lineTop.getHeight();
        mpaddingBottom = lineBottom.getHeight();

        int height = mPaddingTop + mpaddingBottom + body.getHeight();

        mFloatBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mFloatBitmap.eraseColor(Color.TRANSPARENT);
        Canvas canvas = new Canvas(mFloatBitmap);

        canvas.drawBitmap(lineTop, new Rect(0, 0, lineTop.getWidth(), lineTop.getHeight()),
                new Rect(0, 0, width, lineTop.getHeight()), null);

        int topEdge = lineTop.getHeight();

        canvas.drawBitmap(body, new Rect(0, 0, body.getWidth(), body.getHeight()), new Rect(0,
                topEdge,
                width, topEdge + body.getHeight()), null);

        topEdge = topEdge + body.getHeight();
        canvas.drawBitmap(lineBottom,
                new Rect(0, 0, lineBottom.getWidth(), lineBottom.getHeight()), new Rect(0, topEdge,
                        width, topEdge + lineBottom.getHeight()), null);

        v.setDrawingCacheEnabled(false);

        if (mImageView == null) {
            mImageView = new ImageView(mListView.getContext());
        }
        mImageView.setPadding(0, 0, 0, 0);
        mImageView.setImageBitmap(mFloatBitmap);
        mImageView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mImageView.setBackgroundColor(Color.TRANSPARENT);
        return mImageView;
    }

    /**
     * This does nothing
     */
    @Override
    public void onDragFloatView(View floatView, Point position, Point touch) {
        // do nothing
    }

    /**
     * Removes the Bitmap from the ImageView created in
     * onCreateFloatView() and tells the system to recycle it.
     */
    @Override
    public void onDestroyFloatView(View floatView) {
        ((ImageView) floatView).setImageDrawable(null);

        mFloatBitmap.recycle();
        mFloatBitmap = null;
    }


    @Override
    public int getPaddingTop() {
        return mPaddingTop;
    }

    @Override
    public int getPaddingBottom() {
        return mpaddingBottom;
    }


    /**
     * OnCreateFloatView Listener
     *
     */
    public interface OnCreateFloatViewListener {
        public void onCreateFloatView();
    }

    public void setOnCreateFloatViewListener(OnCreateFloatViewListener l) {
        mOnCreateFloatViewListener = l;
    }

    public int getmPaddingTop() {
        return mPaddingTop;
    }

    public int getMpaddingBottom() {
        return mpaddingBottom;
    }

}
