package com.keep100days.principle.common.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.florent37.glidepalette.GlidePalette;
import com.keep100days.principle.Glide4Engine;
import com.keep100days.principle.R;
import com.keep100days.principle.common.util.DensityUtil;
import com.keep100days.principle.common.util.SpUtil;
import com.keep100days.principle.common.util.ToastUtil;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * @author a
 */
public class DYEditActivity extends Activity {

    private static final String KEY_CONTENT = "content";
    private static final String KEY_MARK = "mark";

    private long index;

    private EditText etContent;

    private EditText etMark;

    private EditText etTitle;

    public static void start(Activity activity, String content, String mark) {
        Intent intent = new Intent(activity, DYEditActivity.class);
        intent.putExtra(KEY_CONTENT, content);
        intent.putExtra(KEY_MARK, mark);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dy_edit);

        long lastTime = SpUtil.getLastDYEditTime(this);
        long lastIndex = SpUtil.getLastDYEditIndex(this);
        if (lastIndex == 0) {
            lastIndex = 2;
        }

        etTitle = findViewById(R.id.tv_title);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
        String current = sdf.format(System.currentTimeMillis());
        String last = sdf.format(lastTime);
        if (lastIndex == 2 || TextUtils.equals(last, current)) {
            index = lastIndex;
        } else {
            index = lastIndex + 1;
        }
        etTitle.setText(getString(R.string.dy_title, index));

        etContent = findViewById(R.id.et_content);
        String content = getIntent().getStringExtra(KEY_CONTENT);
        etContent.setText(content);

        etMark = findViewById(R.id.tv_mark);
        String mark = getIntent().getStringExtra(KEY_MARK);
        mark = TextUtils.isEmpty(mark) ? "null" : mark;
        etMark.setText(mark);

        ivBook = findViewById(R.id.iv_book);

        final View root = findViewById(R.id.view_root);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Matisse.from(DYEditActivity.this)
                        .choose(MimeType.ofImage())
                        .countable(false)
                        .maxSelectable(1)
                        .imageEngine(new Glide4Engine())
                        .forResult(3);
            }
        });

        ivBook.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                etContent.setCursorVisible(false);
                etMark.setCursorVisible(false);
                etTitle.setCursorVisible(false);
                Bitmap bitmap = getViewBitmap(root);
                SimpleDateFormat sdf = new SimpleDateFormat("0yyyyMMddHHmmSS", Locale.CHINA);
                saveBitmap(bitmap, sdf.format(System.currentTimeMillis()) + ".png");
                etContent.setCursorVisible(true);
                etMark.setCursorVisible(true);
                etTitle.setCursorVisible(true);
                return false;
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(BASIC_PERMISSION, 1);
        }
    }

    /**
     * 保存方法
     */
    public void saveBitmap(Bitmap bm, String picName) {
        File root = Environment.getExternalStorageDirectory();
        File f = new File(root, picName);
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            ToastUtil.showShort(this, "已经保存:" + f.getAbsolutePath());

            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(f);
            intent.setData(uri);
            sendBroadcast(intent);
            SpUtil.saveLastDYEditTime(this, System.currentTimeMillis());
            String title = etTitle.getText().toString();
            int shapeIndex = title.indexOf("#");
            String strIndex = title.substring(shapeIndex + 1);
            long realIndex = Long.valueOf(strIndex);
            SpUtil.saveLastDYEditIndex(this, realIndex);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            ToastUtil.showShort(this, "保存失败");
        } catch (IOException e) {
            e.printStackTrace();
            ToastUtil.showShort(this, "保存失败");
        }
    }

    public static Bitmap getViewBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private ImageView ivBook;

    private static final String[] BASIC_PERMISSION = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3 && resultCode == RESULT_OK) {
            List<Uri> list = Matisse.obtainResult(data);
            if (list != null && list.size() > 0) {
                Glide.with(this).load(list.get(0))
                        .listener(GlidePalette.with(list.get(0).toString())
                                .use(GlidePalette.Profile.VIBRANT)
                                .intoBackground(findViewById(R.id.view_root))
                                .crossfade(true)
                        )
                        .into(new CustomViewTarget(ivBook) {
                            @Override
                            public void onResourceReady(@NonNull Object resource, @Nullable Transition transition) {
                                BitmapDrawable bitmap = (BitmapDrawable) resource;
                                setupImageViewSize(bitmap);
                                ivBook.setImageDrawable(bitmap);
                            }

                            @Override
                            protected void onResourceCleared(@Nullable Drawable placeholder) {

                            }

                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {

                            }
                        });
            }
        }
    }

    private void setupImageViewSize(BitmapDrawable bitmap) {
        int width = bitmap.getIntrinsicWidth();
        int height = bitmap.getIntrinsicHeight();
        float dstWidth = DensityUtil.getScreenWidth(this) / 2.0f;
        float dstHeight = height * dstWidth / width;
        ivBook.getLayoutParams().width = (int) dstWidth;
        ivBook.getLayoutParams().height = (int) dstHeight;
    }

}