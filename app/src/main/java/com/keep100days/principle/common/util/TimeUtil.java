package com.keep100days.principle.common.util;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author liu
 */
public class TimeUtil {

    private static SimpleDateFormat sDateformatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    /**
     * 当天起始时间戳
     */
    public static long getStartTimeOfToday() {
        long current = System.currentTimeMillis();
        return current / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();
    }

    /**
     * 当天终点时间戳
     */
    public static long getEndTimeOfToday() {
        long start = getStartTimeOfToday();
        return start + 24 * 60 * 60 * 1000 - 1;
    }

    public static boolean isToday(long time) {
        return time <= getEndTimeOfToday() && time >= getStartTimeOfToday();
    }

    public static int getPassedDaysNum(long milliseconds) {
        Date fromDate = new Date(milliseconds - milliseconds % (1000 * 3600 * 24));

        long today = System.currentTimeMillis();
        Date currentData = new Date(today);
        return (int) ((currentData.getTime() - fromDate.getTime()) / (1000 * 3600 * 24));
    }

    public static String getFormattedDate(long milliseconds) {
        synchronized (TextUtils.class) {
            return sDateformatter.format(milliseconds);
        }
    }

    public static String getFormattedTime(long milliseconds) {
        String dataString;

        Date currentTime = new Date(milliseconds);
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        Date todayBegin = todayStart.getTime();
        Date yesterdayBegin = new Date(todayBegin.getTime() - 3600 * 24 * 1000);
        Date preYesterday = new Date(yesterdayBegin.getTime() - 3600 * 24 * 1000);

        if (!currentTime.before(todayBegin)) {
            dataString = "今天";
        } else if (!currentTime.before(yesterdayBegin)) {
            dataString = "昨天";
        } else if (!currentTime.before(preYesterday)) {
            dataString = "前天";
        } else {
            dataString = sDateformatter.format(currentTime);
        }

        return dataString;
    }
}
