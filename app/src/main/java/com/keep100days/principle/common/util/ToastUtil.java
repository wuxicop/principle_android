package com.keep100days.principle.common.util;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.StringRes;
import android.widget.Toast;


public class ToastUtil {

    private static Handler handler = new Handler(Looper.getMainLooper());

    public static void showShort(final Context context, final String txt) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, txt, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void showShort(final Context context, @StringRes final int id) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
