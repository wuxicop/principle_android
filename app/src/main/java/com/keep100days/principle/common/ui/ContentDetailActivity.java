package com.keep100days.principle.common.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.keep100days.principle.R;

/**
 * @author a
 */

public class ContentDetailActivity extends Activity {

    private static final String KEY_CONTENT = "KEY_CONTENT";
    private static final String KEY_MARK = "KEY_MARK";

    public static void start(Activity activity, String content) {
        Intent intent = new Intent(activity, ContentDetailActivity.class);
        intent.putExtra(KEY_CONTENT, content);
        activity.startActivity(intent);
    }

    public static void start(Activity activity, String content, String mark) {
        Intent intent = new Intent(activity, ContentDetailActivity.class);
        intent.putExtra(KEY_CONTENT, content);
        intent.putExtra(KEY_MARK, mark);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_detail);

        final String content = getIntent().getStringExtra(KEY_CONTENT);
        TextView tvContent = findViewById(R.id.tv_content);
        tvContent.setText(content);

        final String mark = getIntent().getStringExtra(KEY_MARK);
        if (!TextUtils.isEmpty(mark)) {
            TextView tvMark = findViewById(R.id.tv_mark);
            tvMark.setText(mark);
        }

        findViewById(R.id.view_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DYEditActivity.start(ContentDetailActivity.this, content, mark);
            }
        });
    }


}
