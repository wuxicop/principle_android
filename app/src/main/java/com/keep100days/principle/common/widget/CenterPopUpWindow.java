package com.keep100days.principle.common.widget;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;

import com.keep100days.principle.R;
import com.keep100days.principle.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author liu
 */
public abstract class CenterPopUpWindow<T> {

    private ListPopupWindow mPopUpWindow;
    private Context context;
    private T model;
    private View anchorView;

    public static final String ACTION_DELETE = "删除";
    public static final String ACTION_UPDATE = "修改";
    public static final String ACTION_TO_DONE = "设为已完成";
    public static final String ACTION_TO_NOT_DONE = "设为未完成";
    public static final String ACTION_TO_STAR = "设为重要";
    public static final String ACTION_TO_NOT_STAR = "设为普通";
    public static final String ACTION_ADD_TO_TODO = "移到今日待做";

    private static final int ACTION_DELETE_POS = 0;
    private static final int ACTION_UPDATE_POS = 1;

    private String[] mUpdate;

    public CenterPopUpWindow(Context context, T list, View anchorView, String... action) {
        this.context = context;
        this.model = list;
        this.anchorView = anchorView;
        mUpdate = action;
        init();
    }

    public abstract void doDelete(T model);

    public abstract void doUpdate(T model);

    public void doCustomActionClick(T model, int position){

    }

    private void init() {
        List<String> mActionList = new ArrayList<>();
        mActionList.add(ACTION_DELETE);
        mActionList.addAll(Arrays.asList(mUpdate));

        mPopUpWindow = new ListPopupWindow(context);
        int width = DensityUtil.dp2px(context, 200);
        mPopUpWindow.setWidth(width);
        mPopUpWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopUpWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        mPopUpWindow.setAnchorView(anchorView);

        int anchorWidth = anchorView.getWidth();
        int offset = (anchorWidth - width) / 2;
        mPopUpWindow.setHorizontalOffset(offset);

        mPopUpWindow.setBackgroundDrawable(context.getDrawable(R.drawable.round_edit_text_bg));
        mPopUpWindow.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, android.R.id.text1, mActionList));
        mPopUpWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                if (pos == ACTION_DELETE_POS) {
                    doDelete(model);
                    hide();
                } else if (pos == ACTION_UPDATE_POS) {
                    doUpdate(model);
                    hide();
                }else {
                    doCustomActionClick(model, pos);
                    hide();
                }
            }
        });
    }

    public void show() {
        if (mPopUpWindow != null && !mPopUpWindow.isShowing()) {
            mPopUpWindow.show();
        }
    }

    public void hide() {
        if (mPopUpWindow != null && mPopUpWindow.isShowing()) {
            mPopUpWindow.dismiss();
        }
    }
}
