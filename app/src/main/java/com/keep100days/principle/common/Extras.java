package com.keep100days.principle.common;

public class Extras {

    public static final String EXTRA_DATE_STRING = "EXTRA_DATE_STRING";
    public static final String EXTRA_DATE_ID = "EXTRA_DATE_ID";
}
