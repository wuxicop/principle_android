package com.keep100days.principle.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.keep100days.principle.common.Constant;
import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.daily.donestate.DailyScoreModel;
import com.keep100days.principle.note.NoteModel;
import com.keep100days.principle.todo.TodoModel;

/**
 * @author a
 */

public class AppDatabaseHelper {
    public static volatile AppDataBase instance;

    public static AppDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDataBase.class, Constant.DB_NAME)
                    .addMigrations(migration_1_2)
                    .addMigrations(migration_2_3)
                    .addMigrations(migration_3_4)
                    .addMigrations(migration_4_5)
                    .addMigrations(migration_5_6)
                    .addMigrations(migration_6_7)
                    .build();
        }
        return instance;
    }

    private static Migration migration_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(CommonModel.getCreateSQL());
        }
    };

    private static Migration migration_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(NoteModel.getAlterSQL());
        }
    };

    private static Migration migration_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(TodoModel.getAlterSQL());
        }
    };

    private static Migration migration_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            String[] alterSQL = DailyScoreModel.getAlterSQL();

            for (String s : alterSQL) {
                database.execSQL(s);
            }
        }
    };

    private static Migration migration_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            String[] alterSQL = DailyScoreModel.getAlterSQL();

            for (String s : alterSQL) {
                database.execSQL(s);
            }
        }
    };

    private static Migration migration_6_7 = new Migration(6, 7) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            String[] alterSQL = CommonModel.getAlterSQL();

            for (String s : alterSQL) {
                database.execSQL(s);
            }
        }
    };
}
