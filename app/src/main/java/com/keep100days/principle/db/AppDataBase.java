package com.keep100days.principle.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDao;
import com.keep100days.principle.daily.DailyDao;
import com.keep100days.principle.daily.DailyModel;
import com.keep100days.principle.daily.donestate.DailyScoreDao;
import com.keep100days.principle.daily.donestate.DailyScoreModel;
import com.keep100days.principle.note.NoteDao;
import com.keep100days.principle.note.NoteModel;
import com.keep100days.principle.principle.PrincipleDao;
import com.keep100days.principle.principle.PrincipleModel;
import com.keep100days.principle.todo.TodoDao;
import com.keep100days.principle.todo.TodoModel;

/**
 * @author liu
 */

@Database(entities = {NoteModel.class, PrincipleModel.class, TodoModel.class, DailyModel.class,
        DailyScoreModel.class, CommonModel.class},
        version = 7)
public abstract class AppDataBase extends RoomDatabase {

    public abstract PrincipleDao principleDao();

    public abstract TodoDao todoDao();

    public abstract NoteDao noteDao();

    public abstract DailyDao dailyDao();

    public abstract DailyScoreDao dailyDoneDao();

    public abstract ModelDao modelDao();

}
