package com.keep100days.principle.eventpool;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.ui.ContentDetailActivity;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.CenterPopUpWindow;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;

import java.util.List;

import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_ADD_TO_TODO;

/**
 * @author a
 */

public class EventPoolAdapter extends BaseAdapter implements DragSortListView.DragSortListener {

    private Activity mActivity;
    private List<CommonModel> mModelList;

    public EventPoolAdapter(Activity activity, List<CommonModel> modelList) {
        mActivity = activity;
        mModelList = modelList;
    }

    @Override
    public int getCount() {
        return mModelList.size();
    }

    @Override
    public CommonModel getItem(int position) {
        return mModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.event_pool_item, null);
            holder = new CommonViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CommonViewHolder) convertView.getTag();
        }

        final CommonModel model = mModelList.get(position);
        int index = position + 1;
        final String content = model.getContent();
        final String result = mActivity.getString(R.string.formatted_item_content, index, content);
        holder.tvContent.setText(result);

        holder.tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentDetailActivity.start(mActivity, content);
            }
        });

        final View itemView = convertView;
        holder.tvContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopUpWindow(model, itemView);
                return true;
            }

        });

        return convertView;
    }

    private ActionListener mActionListener;

    public void setActionListener(ActionListener listener) {
        this.mActionListener = listener;
    }

    public void showPopUpWindow(CommonModel model, View view) {
        CenterPopUpWindow<CommonModel> window = new CenterPopUpWindow<CommonModel>(mActivity, model, view, ACTION_ADD_TO_TODO) {
            @Override
            public void doDelete(CommonModel model) {
                if (mActionListener != null) {
                    mActionListener.doDelete(model);
                }
            }

            @Override
            public void doUpdate(CommonModel model) {
                if (mActionListener != null) {
                    mActionListener.doMoveToToDo(model);
                }
            }
        };
        window.show();
    }

    public void updateData(List<CommonModel> list) {
        mModelList.clear();
        mModelList.addAll(list);
    }

    class CommonViewHolder {
        TextView tvContent;

        CommonViewHolder(View itemView) {
            tvContent = itemView.findViewById(R.id.tv_content);
        }
    }

    @Override
    public void drag(int from, int to) {

    }

    @Override
    public void drop(final int from, final int to) {
        if (from != to) {
            CommonModel model = mModelList.remove(from);
            mModelList.add(to, model);

            ThreadPoolUtils.execute(new Runnable() {
                @Override
                public void run() {
                    ModelDataManager.swapIndex(mActivity, mModelList.get(from), mModelList.get(to));
                }
            });

            notifyDataSetChanged();
        }
    }

    @Override
    public void remove(int which) {
        mModelList.remove(which);
        notifyDataSetChanged();
    }

    public interface ActionListener {

        void doDelete(CommonModel model);

        void doMoveToToDo(CommonModel model);
    }
}
