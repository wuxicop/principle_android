package com.keep100days.principle.eventpool;

import android.app.Activity;

import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;
import com.keep100days.principle.common_model.ModelType;

import java.util.List;

/**
 * @author a
 *
 * 事件池
 */
public class EventPoolPresenter {

    public void doDelete(Activity activity, CommonModel list) {
        ModelDataManager.delete(activity, list);
    }

    public void doRefresh(Activity activity, final EventPoolAdapter adapter) {
        final List<CommonModel> list = ModelDataManager.getAll(activity, ModelType.TYPE_EVENT_POOL);
        if (list != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(list);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void doAddData(Activity activity, String content) {
        CommonModel model = new CommonModel(content, ModelType.TYPE_EVENT_POOL);
        ModelDataManager.insert(activity, model);
    }
}
