package com.keep100days.principle.eventpool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common.widget.dslv.ShadowFloatViewManager;
import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.todo.TodoDataManager;
import com.keep100days.principle.todo.TodoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author a
 */

public class EventPoolActivity extends Activity implements EventPoolAdapter.ActionListener {

    private EventPoolAdapter mAdapter;
    private EventPoolPresenter mPresenter;

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, EventPoolActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_pool);

        mPresenter = new EventPoolPresenter();
        mAdapter = new EventPoolAdapter(this, new ArrayList<CommonModel>());
        mAdapter.setActionListener(this);

        initView();
    }

    private void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(EventPoolActivity.this, mAdapter);
            }
        });
    }

    private EditText editText;

    private void initView() {
        DragSortListView listView = findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));
        listView.setAdapter(mAdapter);

        ShadowFloatViewManager controller = new ShadowFloatViewManager(listView);
        listView.setFloatViewManager(controller);

        editText = findViewById(R.id.edit_input);

        ImageView button = findViewById(R.id.iv_done);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String content = editText.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    addData(content);
                    editText.setText("");
                }
            }
        });
    }

    private void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(EventPoolActivity.this, content);
                mPresenter.doRefresh(EventPoolActivity.this, mAdapter);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        editText.clearFocus();
        refresh();
    }

    @Override
    public void doDelete(final CommonModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doDelete(EventPoolActivity.this, model);
                mPresenter.doRefresh(EventPoolActivity.this, mAdapter);
            }
        });
    }

    @Override
    public void doMoveToToDo(final CommonModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                TodoModel todoModel = new TodoModel(model.getContent());
                TodoDataManager.insert(EventPoolActivity.this, todoModel);
                doDelete(model);
            }
        });
    }
}
