package com.keep100days.principle.annual;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.util.SpUtil;

/**
 * @author a
 */

public class AnnualPlanActivity extends Activity {

    private TextView tvContent;
    private TextView tvEdit;
    private EditText etInput;
    private View bottomLayout;
    private View ivDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annual_plan);
        initViews();
        String annual = SpUtil.getAnnualPlan(this);
        if (TextUtils.isEmpty(annual)) {
            bottomLayout.setVisibility(View.VISIBLE);
        } else {
            tvContent.setText(annual);
            bottomLayout.setVisibility(View.GONE);
        }
    }

    public static void start(Activity activity) {
        Intent intent = new Intent(activity, AnnualPlanActivity.class);
        activity.startActivity(intent);
    }

    private void initViews() {
        tvEdit = findViewById(R.id.tv_edit);
        etInput = findViewById(R.id.et_edit_content);
        tvContent = findViewById(R.id.tv_content);
        bottomLayout = findViewById(R.id.bottom_bar);
        ivDone = findViewById(R.id.iv_done);

        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomLayout.setVisibility(View.VISIBLE);
                etInput.requestFocus();
            }
        });

        ivDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(etInput.getText())) {
                    SpUtil.saveAnnualPlan(AnnualPlanActivity.this, etInput.getText().toString());
                    tvContent.setText(etInput.getText());
                    bottomLayout.setVisibility(View.GONE);
                }
            }
        });
    }
}
