package com.keep100days.principle;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.keep100days.principle.annual.AnnualPlanActivity;
import com.keep100days.principle.common.BaseFragment;
import com.keep100days.principle.common.util.SpUtil;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.daily.DailyFragment;
import com.keep100days.principle.daily.donestate.DailyScoreEditActivity;
import com.keep100days.principle.daily.donestate.scorehistory.DailyScoreHistory;
import com.keep100days.principle.note.NoteFragment;
import com.keep100days.principle.note.window.CopyObserverWindow;
import com.keep100days.principle.principle.PrincipleFragment;
import com.keep100days.principle.todo.TodoFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liu
 */

public class MainActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {

    private static final int POSITION_PRINCIPLE = 0;
    private static final int POSITION_TODO = 1;
    private static final int POSITION_NOTE = 2;
    private static final int POSITION_DAILY = 3;

    private ViewPager mViewPager;
    private BottomNavigationView navigation;

    private TextView mTvTitle;

    private TextView mTvRight1;

    private TextView mTvRight2;

    private MainPrensenter mPrensenter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.tab_principle:
                    mViewPager.setCurrentItem(POSITION_PRINCIPLE);
                    return true;
                case R.id.tab_todo:
                    mViewPager.setCurrentItem(POSITION_TODO);
                    return true;
                case R.id.tab_note:
                    mViewPager.setCurrentItem(POSITION_NOTE);
                    return true;
                case R.id.tab_daily:
                    mViewPager.setCurrentItem(POSITION_DAILY);
                    return true;
                default:
                    break;
            }
            return false;
        }
    };

    private List<Fragment> initFragments() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(PrincipleFragment.newInstance());
        fragments.add(TodoFragment.newInstance());
        fragments.add(NoteFragment.newInstance());
        fragments.add(DailyFragment.newInstance());
        return fragments;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewPager = findViewById(R.id.view_pager);
        mViewPager.addOnPageChangeListener(this);

        mPrensenter = new MainPrensenter();

        List<Fragment> fragmentList = initFragments();
        adapter = new MainAdapter(getSupportFragmentManager(), fragmentList);

        mViewPager.setAdapter(adapter);
        CopyObserverWindow.getInstance(this).register();
        navigation = findViewById(R.id.navigation);
        navigation.setLabelVisibilityMode(1);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mEditText = findViewById(R.id.edit_input);

        ImageView button = findViewById(R.id.iv_done);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String content = mEditText.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    getCurrentFragment().addData(content);
                    mEditText.setText("");
                }
            }
        });

        mTvTitle = findViewById(R.id.tv_title);

        mTvRight1 = findViewById(R.id.tv_right_text1);

        mTvRight2 = findViewById(R.id.tv_right_text2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CopyObserverWindow.getInstance(this).unregister();
    }

    private EditText mEditText;

    @Override
    protected void onResume() {
        super.onResume();
        if (getCurrentFragment() instanceof DailyFragment) {
            refreshTodayScore();
        }
    }

    private void updateTitle(int position) {
        if (position == POSITION_PRINCIPLE) {
            mTvTitle.setText(R.string.tab_principle);
            mTvRight1.setVisibility(View.VISIBLE);
            mTvRight2.setVisibility(View.GONE);
            mTvRight1.setTextColor(Color.TRANSPARENT);
            mTvRight1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleYearPlanViewClick();
                }
            });
            mTvRight1.setText(R.string.annual_plan);
        } else if (position == POSITION_TODO) {
            mTvTitle.setText(R.string.tab_todo);
            mTvRight1.setVisibility(View.VISIBLE);
            mTvRight2.setVisibility(View.VISIBLE);

            mTvRight1.setText(R.string.only_show_not_done);
            mTvRight2.setText(R.string.only_show_today);

            mTvRight1.setTextColor(getResources().getColorStateList(R.color.selector_checkable_text_color));
            mTvRight2.setTextColor(getResources().getColorStateList(R.color.selector_checkable_text_color));

            boolean onlyNotDone = SpUtil.isOnlyShowNotDoneTodo(this);
            mTvRight1.setSelected(onlyNotDone);

            mTvRight1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final boolean onlyNotDone = !mTvRight1.isSelected();
                    SpUtil.saveIsOnlyShowNotDoneTodo(MainActivity.this, onlyNotDone);
                    mTvRight1.setSelected(onlyNotDone);
                    getCurrentFragment().refresh();
                }
            });

            boolean onlyToday = SpUtil.isOnlyShowTodayTodo(this);
            mTvRight2.setSelected(onlyToday);

            mTvRight2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final boolean onlyToday = !mTvRight2.isSelected();
                    SpUtil.saveIsOnlyShowTodayTodo(MainActivity.this, onlyToday);
                    mTvRight2.setSelected(onlyToday);
                    getCurrentFragment().refresh();
                }
            });

        } else if (position == POSITION_NOTE) {
            mTvTitle.setText(R.string.tab_note);
            mTvRight1.setVisibility(View.GONE);
            mTvRight2.setVisibility(View.GONE);
        } else if (position == POSITION_DAILY) {
            mTvTitle.setText(R.string.tab_daily);
            mTvRight1.setVisibility(View.VISIBLE);
            mTvRight2.setVisibility(View.VISIBLE);

            mTvRight1.setText(getString(R.string.score_today, "X"));
            mTvRight2.setText(R.string.history_score);

            refreshTodayScore();

            mTvRight1.setTextColor(Color.WHITE);
            mTvRight2.setTextColor(getResources().getColor(R.color.color_text_not_selected));

            mTvRight1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DailyScoreEditActivity.start(MainActivity.this);
                }
            });

            mTvRight2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DailyScoreHistory.start(MainActivity.this);
                }
            });

        }
    }

    private void refreshTodayScore() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                final String score = mPrensenter.getTodayScore(MainActivity.this);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mTvRight1.setText(getString(R.string.score_today, score));
                    }
                });
            }
        });
    }


    private static final long DURATION = 5 * 1000;
    private static final long MIN_COUNT = 3;
    private long startTime = 0;
    private int clickCount = 0;

    private void handleYearPlanViewClick() {
        if (startTime == 0) {
            startTime = System.currentTimeMillis();
            clickCount++;
        } else {
            long time = System.currentTimeMillis();
            if (time - startTime <= DURATION) {
                if (++clickCount >= MIN_COUNT) {
                    AnnualPlanActivity.start(this);
                    startTime = 0;
                    clickCount = 0;
                }
            } else {
                startTime = 0;
                clickCount = 0;
            }
        }
    }

    private BaseFragment getCurrentFragment() {
        int currentItem = mViewPager.getCurrentItem();
        return (BaseFragment) adapter.getItem(currentItem);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        navigation.setOnNavigationItemSelectedListener(null);
        navigation.setSelectedItemId(getItemId(position));
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        updateTitle(position);
    }

    private int getItemId(int position) {
        if (position == POSITION_PRINCIPLE) {
            return R.id.tab_principle;
        } else if (position == POSITION_TODO) {
            return R.id.tab_todo;
        } else if (position == POSITION_NOTE) {
            return R.id.tab_note;
        } else {
            return R.id.tab_daily;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}
