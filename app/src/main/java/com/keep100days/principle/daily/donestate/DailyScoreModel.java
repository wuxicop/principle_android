package com.keep100days.principle.daily.donestate;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.keep100days.principle.common.util.TimeUtil;

/**
 * @author liu
 */

@Entity(tableName = "daily_score_table")
public class DailyScoreModel {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "create_time")
    private long createTime;

    @ColumnInfo(name = "update_time")
    private long updateTime;

    private String comment;

    /**
     * 年-月-日
     */
    @ColumnInfo(name = "date_str")
    private String date;

    private int score;

    public DailyScoreModel(int score, String comment) {
        setScore(score);
        setComment(comment);
        long time = System.currentTimeMillis();
        setCreateTime(time);
        setUpdateTime(time);
        setDate(TimeUtil.getFormattedDate(time));
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public static String[] getAlterSQL() {
        return new String[]{
                "DROP TABLE daily_done_state_table",
                "CREATE TABLE IF NOT EXISTS daily_score_table ( "
                        + "id INTEGER PRIMARY KEY NOT NULL,"
                        + "create_time INTEGER NOT NULL,"
                        + "update_time INTEGER NOT NULL,"
                        + "score INTEGER NOT NULL,"
                        + "comment TEXT,"
                        + "date_str TEXT"
                        + " );"
        };
    }
}
