package com.keep100days.principle.daily.donestate;

import android.content.Context;

import java.util.List;

/**
 * @author a
 */
public class DailyScorePresenter {

    public DailyScoreModel loadDataByData(Context context, String date) {
        DailyScoreModel model = null;
        List<DailyScoreModel> scoreModels = DailyScoreDataManager.query(context, date);
        if (scoreModels != null && scoreModels.size() > 0) {
            model = scoreModels.get(0);
        }

        return model;
    }
}
