package com.keep100days.principle.daily.donestate;

import android.content.Context;

import com.keep100days.principle.db.AppDatabaseHelper;

import java.util.List;

/**
 * @author liu
 */

public class DailyScoreDataManager {

    public static List<DailyScoreModel> getAll(Context context) {
        try {
            return AppDatabaseHelper.getInstance(context).dailyDoneDao().getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void insert(Context context, DailyScoreModel model) {
        try {
            AppDatabaseHelper.getInstance(context).dailyDoneDao().insertRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, DailyScoreModel model) {
        try {
            AppDatabaseHelper.getInstance(context).dailyDoneDao().updateRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<DailyScoreModel> query(Context context, String date) {
        try {
            return AppDatabaseHelper.getInstance(context).dailyDoneDao().query(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
