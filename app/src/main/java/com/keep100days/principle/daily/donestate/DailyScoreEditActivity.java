package com.keep100days.principle.daily.donestate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.util.SoftKeyUtil;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.util.TimeUtil;
import com.keep100days.principle.common.util.ToastUtil;

/**
 * @author a
 */
public class DailyScoreEditActivity extends Activity {

    private RatingBar mRatingBar;
    private EditText mEditText;
    private TextView mTvSimpleMark;
    private String mCurrentDate;
    private DailyScorePresenter mPresenter;
    private DailyScoreModel mOriginalModel;

    private static final float PASS_SCORE = 3.0F;
    private static final float OK_SCORE = 4.0F;
    private static final float EXCELLENT_SCORE = 5.0F;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_socre_edit);
        init();
    }

    private void init() {
        mRatingBar = findViewById(R.id.rating_bar);
        mEditText = findViewById(R.id.edit_text);
        mTvSimpleMark = findViewById(R.id.tv_simple_mark);
        mPresenter = new DailyScorePresenter();

        mCurrentDate = TimeUtil.getFormattedDate(System.currentTimeMillis());

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating < PASS_SCORE) {
                    mTvSimpleMark.setText(R.string.bad);
                } else if (rating >= PASS_SCORE && rating < OK_SCORE) {
                    mTvSimpleMark.setText(R.string.pass);
                } else if (rating >= OK_SCORE && rating < EXCELLENT_SCORE) {
                    mTvSimpleMark.setText(R.string.good);
                } else if (rating >= EXCELLENT_SCORE) {
                    mTvSimpleMark.setText(R.string.excellent);
                }
            }
        });

        TextView textView = findViewById(R.id.tv_title);
        textView.setText(R.string.score);

        TextView right1 = findViewById(R.id.tv_right_text1);
        right1.setVisibility(View.VISIBLE);
        right1.setText(R.string.save);

        right1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int score = (int) mRatingBar.getRating();
                String comment = mEditText.getText().toString();
                if (mOriginalModel == null) {
                    mOriginalModel = new DailyScoreModel(score, comment);
                } else {
                    mOriginalModel.setComment(comment);
                    mOriginalModel.setScore(score);
                }
                ThreadPoolUtils.execute(new Runnable() {
                    @Override
                    public void run() {
                        DailyScoreDataManager.insert(DailyScoreEditActivity.this, mOriginalModel);
                        ToastUtil.showShort(DailyScoreEditActivity.this, R.string.save_success);
                        SoftKeyUtil.hideIME(DailyScoreEditActivity.this, mEditText);
                        finish();
                    }
                });
            }
        });

        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mOriginalModel = mPresenter.loadDataByData(DailyScoreEditActivity.this, mCurrentDate);
                if (mOriginalModel != null) {
                    mRatingBar.setRating(mOriginalModel.getScore());
                    mEditText.setText(mOriginalModel.getComment());
                }
            }
        });

    }

    public static void start(Context context) {
        Intent intent = new Intent(context, DailyScoreEditActivity.class);
        context.startActivity(intent);
    }

}
