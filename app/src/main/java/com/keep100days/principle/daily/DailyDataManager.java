package com.keep100days.principle.daily;

import android.content.Context;

import com.keep100days.principle.db.AppDatabaseHelper;

import java.util.List;

/**
 * @author liu
 */

public class DailyDataManager {

    public static List<DailyModel> getAll(Context context) {
        try {
            return AppDatabaseHelper.getInstance(context).dailyDao().getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long insert(Context context, DailyModel model) {
        try {
           long id = AppDatabaseHelper.getInstance(context).dailyDao().insertRecord(model);
            updateIndexById(context, id, id);
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void delete(Context context, DailyModel model) {
        try {
            AppDatabaseHelper.getInstance(context).dailyDao().deleteRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, DailyModel model) {
        try {
            AppDatabaseHelper.getInstance(context).dailyDao().updateRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateIndexById(Context context, long id, long index) {
        try {
            AppDatabaseHelper.getInstance(context).dailyDao().updateRecordIndexById(id, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void swapIndex(Context context, DailyModel left, DailyModel right) {
        long leftIndex = left.getIndex();

        updateIndexById(context, left.getId(), right.getIndex());
        updateIndexById(context, right.getId(), leftIndex);
    }
}
