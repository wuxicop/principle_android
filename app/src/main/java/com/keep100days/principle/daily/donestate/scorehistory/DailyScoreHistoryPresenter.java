package com.keep100days.principle.daily.donestate.scorehistory;

import android.content.Context;

import com.keep100days.principle.daily.donestate.DailyScoreDataManager;
import com.keep100days.principle.daily.donestate.DailyScoreModel;

import java.util.List;

public class DailyScoreHistoryPresenter {

    public DailyScoreModel loadDataByData(Context context, String date) {
        DailyScoreModel model = null;
        List<DailyScoreModel> scoreModels = DailyScoreDataManager.query(context, date);
        if (scoreModels != null && scoreModels.size() > 0) {
            model = scoreModels.get(0);
        }

        return model;
    }
}
