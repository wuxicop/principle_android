package com.keep100days.principle.daily;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.ui.ContentDetailActivity;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.CenterPopUpWindow;
import com.keep100days.principle.common.widget.dslv.DragSortListView;

import java.util.List;

import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_UPDATE;

/**
 * @author liu
 */

public class DailyAdapter extends BaseAdapter implements DragSortListView.DragSortListener {

    private Activity mContext;
    private List<DailyModel> mList;
    private DailyAdapter.ActionListener mActionListener;

    public DailyAdapter(Activity context, List<DailyModel> list) {
        mContext = context;
        mList = list;
    }

    public void setActionListener(DailyAdapter.ActionListener listener) {
        this.mActionListener = listener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public DailyModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NoteViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.daily_item, parent, false);
            holder = new NoteViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (NoteViewHolder) convertView.getTag();
        }

        final DailyModel model = mList.get(position);
        int index = position + 1;
        final String content = model.getContent();
        final String result = mContext.getString(R.string.formatted_item_content, index, content);
        holder.tvContent.setText(result);

        if (model.isDone()) {
            holder.tvContent.setAlpha(0.4f);
        } else {
            holder.tvContent.setAlpha(1.0f);
        }

        holder.tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentDetailActivity.start(mContext, content);
            }
        });
        final View itemView = convertView;
        holder.tvContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopUpWindow(model, itemView);
                return true;
            }
        });

        return convertView;
    }


    public void showPopUpWindow(DailyModel model, View view) {
        CenterPopUpWindow<DailyModel> window = new CenterPopUpWindow<DailyModel>(mContext, model, view, ACTION_UPDATE) {
            @Override
            public void doDelete(DailyModel model) {
                if (mActionListener != null) {
                    mActionListener.doDelete(model);
                }
            }

            @Override
            public void doUpdate(DailyModel model) {
                if (mActionListener != null) {
                    mActionListener.doUpdate(model);
                }
            }
        };
        window.show();
    }


    @Override
    public void drag(int from, int to) {

    }

    @Override
    public void drop(final int from, final int to) {
        if (from != to) {
            DailyModel model = mList.remove(from);
            mList.add(to, model);

            ThreadPoolUtils.execute(new Runnable() {
                @Override
                public void run() {
                    DailyDataManager.swapIndex(mContext, mList.get(from), mList.get(to));
                }
            });

            notifyDataSetChanged();
        }
    }

    @Override
    public void remove(int which) {
        mList.remove(which);
        notifyDataSetChanged();
    }

    public void updateData(List<DailyModel> list) {
        mList.clear();
        mList.addAll(list);
    }

    class NoteViewHolder {
        TextView tvContent;

        NoteViewHolder(@NonNull View itemView) {
            tvContent = itemView.findViewById(R.id.tv_daily_content);
        }
    }

    public interface ActionListener {

        void doDelete(DailyModel model);

        void doUpdate(DailyModel model);
    }
}