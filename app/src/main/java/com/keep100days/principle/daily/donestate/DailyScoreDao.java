package com.keep100days.principle.daily.donestate;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * @author liu
 */

@Dao
public interface DailyScoreDao {

    @Query("select * FROM daily_score_table order by id asc")
    List<DailyScoreModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecord(DailyScoreModel entity);

    @Delete()
    int deleteRecord(DailyScoreModel entity);

    @Update()
    int updateRecord(DailyScoreModel entity);

    @Query("select * FROM daily_score_table where date_str =:date")
    List<DailyScoreModel> query(String date);

}
