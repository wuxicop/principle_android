package com.keep100days.principle.daily;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keep100days.principle.R;
import com.keep100days.principle.common.BaseFragment;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common.widget.dslv.ShadowFloatViewManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author a
 */

public class DailyFragment extends BaseFragment implements DailyAdapter.ActionListener {

    public static final String TAG = "DailyFragment";

    private DailyAdapter mAdapter;

    private DailyPresenter mPresenter;

    public static DailyFragment newInstance() {
        return new DailyFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new DailyPresenter();

        mAdapter = new DailyAdapter(getActivity(), new ArrayList<DailyModel>());
        mAdapter.setActionListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(getActivity(), content);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_daily, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DragSortListView listView = view.findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(getActivity()));
        listView.addFooterView(new View(getActivity()));
        listView.setAdapter(mAdapter);

        ShadowFloatViewManager controller = new ShadowFloatViewManager(listView);
        listView.setFloatViewManager(controller);
    }

    @Override
    public void doDelete(final DailyModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doDelete(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void doUpdate(final DailyModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doUpdate(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

}
