package com.keep100days.principle.daily;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * @author liu
 */

@Dao
public interface DailyDao {

    @Query("select * FROM daily_table order by `index` asc")
    List<DailyModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertRecord(DailyModel entity);

    @Delete()
    int deleteRecord(DailyModel entity);

    @Update()
    int updateRecord(DailyModel entity);

    @Query("update daily_table set `index`=:index where id =:id")
    void updateRecordIndexById(long id, long index);
}
