package com.keep100days.principle.daily.donestate.scorehistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.CalendarView;
import com.keep100days.principle.R;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.util.TimeUtil;
import com.keep100days.principle.daily.donestate.DailyScoreModel;

public class DailyScoreHistory extends Activity {

    private TextView tvCurrentMonth;
    private TextView tvComment;
    private CalendarView mCalendarView;
    private DailyScoreHistoryPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_score_history);
        init();
    }

    private void init() {

        mPresenter = new DailyScoreHistoryPresenter();

        tvComment = findViewById(R.id.tv_comment);

        tvCurrentMonth = findViewById(R.id.tv_center);

        tvCurrentMonth.setVisibility(View.VISIBLE);

        mCalendarView = findViewById(R.id.calendar_view);

        mCalendarView.setOnMonthChangeListener(new CalendarView.OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month) {
                tvCurrentMonth.setText(getString(R.string.current_month, year, month));
            }
        });

        mCalendarView.setOnCalendarSelectListener(new CalendarView.OnCalendarSelectListener() {
            @Override
            public void onCalendarOutOfRange(Calendar calendar) {

            }

            @Override
            public void onCalendarSelect(final Calendar calendar, boolean isClick) {
                long selectedTime = calendar.getTimeInMillis();
                final String date = TimeUtil.getFormattedDate(selectedTime);
                ThreadPoolUtils.execute(new Runnable() {
                    @Override
                    public void run() {
                        DailyScoreModel model = mPresenter.loadDataByData(DailyScoreHistory.this, date);
                        final String comment = model == null ? "" : model.getComment();
                        int score = model == null ? -1 : model.getScore();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvComment.setText(comment);
                            }
                        });

                    }
                });

            }
        });

    }

    public static void start(Context context) {
        Intent intent = new Intent(context, DailyScoreHistory.class);
        context.startActivity(intent);
    }
}
