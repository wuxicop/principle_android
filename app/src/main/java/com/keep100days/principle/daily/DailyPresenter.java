package com.keep100days.principle.daily;

import android.app.Activity;
import android.content.Context;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author liu
 */

public class DailyPresenter {

    public void doRefresh(Activity activity, final DailyAdapter adapter) {
        final List<DailyModel> list = DailyDataManager.getAll(activity);
        if (list != null) {
            sortItems(list);

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(list);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void sortItems(List<DailyModel> list) {
        Collections.sort(list, new Comparator<DailyModel>() {
            @Override
            public int compare(DailyModel left, DailyModel right) {
                return getCompareWeight(left) - getCompareWeight(right);
            }
        });
    }

    private int getCompareWeight(DailyModel model) {
        if (model == null) {
            return Integer.MAX_VALUE;
        }
        return model.isDone() ? 1 : 0;
    }

    public void doAddData(Activity activity, String content) {
        DailyModel model = new DailyModel(content);
        DailyDataManager.insert(activity, model);
    }

    public void doDelete(Activity activity, DailyModel model) {
        DailyDataManager.delete(activity, model);
    }

    public void doUpdate(Context context, DailyModel model) {
        DailyDataManager.update(context, model);
    }
}
