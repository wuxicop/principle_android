package com.keep100days.principle;

import android.content.Context;

import com.keep100days.principle.common.util.TimeUtil;
import com.keep100days.principle.daily.donestate.DailyScoreDataManager;
import com.keep100days.principle.daily.donestate.DailyScoreModel;

import java.util.List;

public class MainPrensenter {

    public String getTodayScore(Context context) {
        String date = TimeUtil.getFormattedDate(System.currentTimeMillis());
        List<DailyScoreModel> scoreModels = DailyScoreDataManager.query(context, date);
        if (scoreModels != null && scoreModels.size() > 0) {
            return scoreModels.get(0).getScore() + "";
        } else {
            return "X";
        }
    }
}
