package com.keep100days.principle.principle;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.ui.ContentDetailActivity;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.CenterPopUpWindow;
import com.keep100days.principle.common.widget.dslv.DragSortListView;

import java.util.List;

import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_UPDATE;

/**
 * @author liu
 */
public class PrincipleAdapter extends BaseAdapter implements DragSortListView.DragSortListener {

    private Activity mContext;
    private List<PrincipleModel> mList;
    private ActionListener mActionListener;

    public PrincipleAdapter(Activity context, List<PrincipleModel> list) {
        mContext = context;
        mList = list;
    }

    public void setActionListener(ActionListener listener) {
        this.mActionListener = listener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PrincipleViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.principle_item, null);
            holder = new PrincipleViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PrincipleViewHolder) convertView.getTag();
        }


        final PrincipleModel model = mList.get(position);
        int index = position + 1;
        final String content = model.getContent();
        final String result = mContext.getString(R.string.formatted_item_content, index, content);
        holder.tvContent.setText(result);

        holder.tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentDetailActivity.start(mContext, content);
            }
        });
        final View itemView = convertView;
        holder.tvContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopUpWindow(model, itemView);
                return true;
            }

        });

        return convertView;
    }

    public void showPopUpWindow(PrincipleModel model, View view) {
        CenterPopUpWindow<PrincipleModel> window = new CenterPopUpWindow<PrincipleModel>(mContext, model, view, ACTION_UPDATE) {
            @Override
            public void doDelete(PrincipleModel model) {
                if (mActionListener != null) {
                    mActionListener.doDelete(model);
                }
            }

            @Override
            public void doUpdate(PrincipleModel model) {
                if (mActionListener != null) {
                    mActionListener.doUpdate(model);
                }
            }
        };
        window.show();
    }

    @Override
    public void drag(int from, int to) {

    }

    @Override
    public void drop(final int from, final int to) {
        if (from != to) {
            PrincipleModel model = mList.remove(from);
            mList.add(to, model);

            ThreadPoolUtils.execute(new Runnable() {
                @Override
                public void run() {
                    PrincipleDataManager.swapIndex(mContext, mList.get(from), mList.get(to));
                }
            });

            notifyDataSetChanged();
        }
    }

    @Override
    public void remove(int which) {
        mList.remove(which);
        notifyDataSetChanged();
    }

    public void updateData(List<PrincipleModel> list) {
        mList.clear();
        mList.addAll(list);
    }

    class PrincipleViewHolder {
        TextView tvContent;

        PrincipleViewHolder(View itemView) {
            tvContent = itemView.findViewById(R.id.tv_principle_content);
        }
    }

    public interface ActionListener {

        void doDelete(PrincipleModel model);

        void doUpdate(PrincipleModel model);
    }
}
