package com.keep100days.principle.principle;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.keep100days.principle.principle.PrincipleModel;

import java.util.List;

@Dao
public interface PrincipleDao {

    @Query("select * FROM principle_table order by `index` asc")
    List<PrincipleModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertRecord(PrincipleModel entity);

    @Delete()
    int deleteRecord(PrincipleModel entity);

    @Query("update principle_table set content=:content where id =:id")
    int updateRecordContentById(long id, String content);

    @Query("update principle_table set `index`=:index where id =:id")
    void updateRecordIndexById(long id, long index);

}
