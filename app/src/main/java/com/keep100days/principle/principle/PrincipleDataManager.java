package com.keep100days.principle.principle;

import android.content.Context;

import com.keep100days.principle.db.AppDatabaseHelper;

import java.util.List;

/**
 * @author liu
 */
public class PrincipleDataManager {

    public static List<PrincipleModel> getAll(Context context) {
        try {
            return AppDatabaseHelper.getInstance(context).principleDao().getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void insert(Context context, PrincipleModel model) {
        try {
            long id = AppDatabaseHelper.getInstance(context).principleDao().insertRecord(model);
            updateIndexById(context, id, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void delete(Context context, PrincipleModel model) {
        try {
            AppDatabaseHelper.getInstance(context).principleDao().deleteRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateIndexById(Context context, long id, long index) {
        try {
            AppDatabaseHelper.getInstance(context).principleDao().updateRecordIndexById(id, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, long id, String content) {
        try {
            AppDatabaseHelper.getInstance(context).principleDao().updateRecordContentById(id, content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void swapIndex(Context context, PrincipleModel left, PrincipleModel right) {
        long leftIndex = left.getIndex();

        updateIndexById(context, left.getId(), right.getIndex());
        updateIndexById(context, right.getId(), leftIndex);
    }
}
