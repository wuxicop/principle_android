package com.keep100days.principle.principle;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.keep100days.principle.R;
import com.keep100days.principle.annual.AnnualPlanActivity;
import com.keep100days.principle.common.BaseFragment;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common.widget.dslv.ShadowFloatViewManager;

import java.util.ArrayList;

/**
 * @author liu
 */
public class PrincipleFragment extends BaseFragment implements PrincipleAdapter.ActionListener {

    public static final String TAG = "PrincipleFragment";

    private PrincipleAdapter mAdapter;

    private PrinciplePresenter mPresenter;

    public static PrincipleFragment newInstance() {
        return new PrincipleFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new PrinciplePresenter();

        mAdapter = new PrincipleAdapter(getActivity(), new ArrayList<PrincipleModel>());
        mAdapter.setActionListener(this);
    }

    @Override
    public void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(getActivity(), content);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_principle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DragSortListView listView = view.findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(getActivity()));
        listView.addFooterView(new View(getActivity()));
        listView.setAdapter(mAdapter);

        ShadowFloatViewManager controller = new ShadowFloatViewManager(listView);
        listView.setFloatViewManager(controller);
    }

    @Override
    public void doDelete(final PrincipleModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doDelete(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void doUpdate(final PrincipleModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doUpdateContentById(getActivity(), model.getId(), model.getContent());
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }
}
