package com.keep100days.principle.principle;

import android.app.Activity;
import android.content.Context;

import java.util.List;

/**
 * @author liu
 */
public class PrinciplePresenter {

    public void doRefresh(Activity activity, final PrincipleAdapter adapter) {
        final List<PrincipleModel> list = PrincipleDataManager.getAll(activity);
        if (list != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(list);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void doAddData(Activity activity, String content) {
        PrincipleModel model = new PrincipleModel(content);
        PrincipleDataManager.insert(activity, model);
    }

    public void doDelete(Activity activity, PrincipleModel model) {
        PrincipleDataManager.delete(activity, model);
    }

    public void doUpdateContentById(Context context, long id, String content) {
        PrincipleDataManager.update(context, id, content);
    }
}
