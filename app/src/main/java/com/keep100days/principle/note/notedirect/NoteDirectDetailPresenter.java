package com.keep100days.principle.note.notedirect;

import android.app.Activity;

import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;
import com.keep100days.principle.common_model.ModelType;

import java.util.List;

class NoteDirectDetailPresenter {

    public void doRefresh(Activity activity, long directId, final NoteDirectDetailAdapter adapter) {
        final List<CommonModel> list = ModelDataManager.query(activity, directId, ModelType.TYPE_NOTE_DIRECT_ITEM);
        if (list != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(list);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void doAddData(Activity activity, long directId, String content) {
        CommonModel model = new CommonModel(content, ModelType.TYPE_NOTE_DIRECT_ITEM);
        model.setCommonInteger((int) directId);
        ModelDataManager.insert(activity, model);
        //对应的文件夹数字加1
        List<CommonModel> list = ModelDataManager.query(activity, directId);
        if (list != null && list.size() > 0) {
            CommonModel direct = list.get(0);
            direct.setCommonInteger(direct.getCommonInteger() + 1);
            ModelDataManager.update(activity, direct);
        }
    }

    public void doDelete(Activity activity, long directId, CommonModel model) {
        ModelDataManager.delete(activity, model);
        //对应的文件夹数字减1
        List<CommonModel> list = ModelDataManager.query(activity, directId);
        if (list != null && list.size() > 0) {
            CommonModel direct = list.get(0);
            direct.setCommonInteger(direct.getCommonInteger() - 1);
            ModelDataManager.update(activity, direct);
        }
    }
}
