package com.keep100days.principle.note.notedirect;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.Extras;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common.widget.dslv.ShadowFloatViewManager;
import com.keep100days.principle.common_model.CommonModel;

import java.util.ArrayList;

/**
 * @author a
 */

public class NoteDirectDetailActivity extends Activity {

    private long mDirectId;

    private TextView mTvCenter;

    private NoteDirectDetailAdapter mAdapter;

    private NoteDirectDetailPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_dirct_detail);
        mPresenter = new NoteDirectDetailPresenter();
        mDirectId = getIntent().getLongExtra(Extras.EXTRA_DATE_ID, 0);
        String directName = getIntent().getStringExtra(Extras.EXTRA_DATE_STRING);
        mTvCenter = findViewById(R.id.tv_center);
        mTvCenter.setText(directName);
        mAdapter = new NoteDirectDetailAdapter(this, new ArrayList<CommonModel>());
        mAdapter.setActionListener(new NoteDirectDetailAdapter.ActionListener() {
            @Override
            public void doDelete(final CommonModel model) {
                ThreadPoolUtils.execute(new Runnable() {
                    @Override
                    public void run() {
                        mPresenter.doDelete(NoteDirectDetailActivity.this, mDirectId, model);
                        mPresenter.doRefresh(NoteDirectDetailActivity.this, mDirectId, mAdapter);
                    }
                });
            }

            @Override
            public void doUpdate(CommonModel model) {

            }
        });
        initView();
    }

    public static void start(Activity context, long id, String name) {
        Intent intent = new Intent(context, NoteDirectDetailActivity.class);
        intent.putExtra(Extras.EXTRA_DATE_ID, id);
        intent.putExtra(Extras.EXTRA_DATE_STRING, name);
        context.startActivity(intent);
    }


    private EditText editText;

    private void initView() {
        DragSortListView listView = findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));
        listView.setAdapter(mAdapter);

        ShadowFloatViewManager controller = new ShadowFloatViewManager(listView);
        listView.setFloatViewManager(controller);

        editText = findViewById(R.id.edit_input);

        ImageView button = findViewById(R.id.iv_done);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String content = editText.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    addData(content);
                    editText.setText("");
                }
            }
        });
    }

    private void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(NoteDirectDetailActivity.this, mDirectId, content);
                mPresenter.doRefresh(NoteDirectDetailActivity.this, mDirectId, mAdapter);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(NoteDirectDetailActivity.this, mDirectId, mAdapter);
            }
        });
    }

}
