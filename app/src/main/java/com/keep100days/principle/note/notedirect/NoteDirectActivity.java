package com.keep100days.principle.note.notedirect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common.widget.dslv.ShadowFloatViewManager;
import com.keep100days.principle.common_model.CommonModel;

import java.util.ArrayList;

/**
 * @author a
 */

public class NoteDirectActivity extends Activity {

    private NoteDirectAdapter mAdapter;

    private NoteDirectPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_dirct);
        mPresenter = new NoteDirectPresenter();
        mAdapter = new NoteDirectAdapter(this, new ArrayList<CommonModel>());
        mAdapter.setActionListener(new NoteDirectAdapter.ActionListener() {
            @Override
            public void doDelete(final CommonModel model) {
                ThreadPoolUtils.execute(new Runnable() {
                    @Override
                    public void run() {
                        mPresenter.doDelete(NoteDirectActivity.this, model);
                        mPresenter.doRefresh(NoteDirectActivity.this, mAdapter);
                    }
                });
            }

            @Override
            public void doUpdate(CommonModel model) {

            }
        });
        initView();
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, NoteDirectActivity.class);
        context.startActivity(intent);
    }

    private EditText editText;

    private void initView() {
        DragSortListView listView = findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));
        listView.setAdapter(mAdapter);

        ShadowFloatViewManager controller = new ShadowFloatViewManager(listView);
        listView.setFloatViewManager(controller);

        editText = findViewById(R.id.edit_input);

        ImageView button = findViewById(R.id.iv_done);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String content = editText.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    addData(content);
                    editText.setText("");
                }
            }
        });
    }

    private void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(NoteDirectActivity.this, content);
                mPresenter.doRefresh(NoteDirectActivity.this, mAdapter);
            }
        });
    }

    private void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(NoteDirectActivity.this, mAdapter);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }
}
