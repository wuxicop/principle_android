package com.keep100days.principle.note.notedirect;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.ui.ContentDetailActivity;
import com.keep100days.principle.common.widget.CenterPopUpWindow;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common_model.CommonModel;

import java.util.List;

import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_UPDATE;

/**
 * @author a
 */

public class NoteDirectAdapter extends BaseAdapter implements DragSortListView.DragSortListener {

    private Activity mContext;
    private List<CommonModel> mList;
    private ActionListener mActionListener;

    public NoteDirectAdapter(Activity context, List<CommonModel> list) {
        mContext = context;
        mList = list;
    }

    public void setActionListener(ActionListener listener) {
        this.mActionListener = listener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NoteDirectViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.note_direct_item, null);
            holder = new NoteDirectViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (NoteDirectViewHolder) convertView.getTag();
        }

        final CommonModel model = mList.get(position);
        int index = position + 1;
        final String content = model.getContent();
        final String result = mContext.getString(R.string.formatted_item_content, index, content);

        holder.tvName.setText(result);

        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoteDirectDetailActivity.start(mContext, model.getId(), content);
            }
        });

        final View itemView = convertView;

        holder.tvName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopUpWindow(model, itemView);
                return true;
            }
        });

        holder.tvNum.setText(String.valueOf(model.getCommonInteger()));

        return convertView;
    }

    public void showPopUpWindow(CommonModel model, View view) {
        CenterPopUpWindow<CommonModel> window = new CenterPopUpWindow<CommonModel>(mContext, model, view, ACTION_UPDATE) {
            @Override
            public void doDelete(CommonModel model) {
                if (mActionListener != null) {
                    mActionListener.doDelete(model);
                }
            }

            @Override
            public void doUpdate(CommonModel model) {
                if (mActionListener != null) {
                    mActionListener.doUpdate(model);
                }
            }
        };
        window.show();
    }

    @Override
    public void drag(int from, int to) {

    }

    @Override
    public void drop(final int from, final int to) {
        if (from != to) {
            CommonModel model = mList.remove(from);
            mList.add(to, model);

            notifyDataSetChanged();
        }
    }

    @Override
    public void remove(int which) {
        mList.remove(which);
        notifyDataSetChanged();
    }

    public void updateData(List<CommonModel> list) {
        mList.clear();
        mList.addAll(list);
    }

    class NoteDirectViewHolder {
        TextView tvName;
        TextView tvNum;

        NoteDirectViewHolder(View itemView) {
            tvName = itemView.findViewById(R.id.tv_note_direct_name);
            tvNum = itemView.findViewById(R.id.tv_item_num);
        }
    }

    public interface ActionListener {

        void doDelete(CommonModel model);

        void doUpdate(CommonModel model);
    }
}
