package com.keep100days.principle.note.notedirect;

import android.app.Activity;

import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;
import com.keep100days.principle.common_model.ModelType;

import java.util.List;

/**
 * @author a
 */

public class NoteDirectPresenter {

    public void doRefresh(Activity activity, final NoteDirectAdapter adapter) {
        final List<CommonModel> list = ModelDataManager.getAll(activity, ModelType.TYPE_NOTE_DIRECT);
        if (list != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(list);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void doAddData(Activity activity, String content) {
        CommonModel model = new CommonModel(content, ModelType.TYPE_NOTE_DIRECT);
        ModelDataManager.insert(activity, model);
    }

    public void doDelete(Activity activity, CommonModel model) {
        ModelDataManager.delete(activity, model);
    }
}
