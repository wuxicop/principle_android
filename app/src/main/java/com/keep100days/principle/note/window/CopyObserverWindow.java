package com.keep100days.principle.note.window;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.keep100days.principle.R;
import com.keep100days.principle.common.util.DensityUtil;
import com.keep100days.principle.common.util.SoftKeyUtil;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;
import com.keep100days.principle.common_model.ModelType;
import com.keep100days.principle.note.NoteDataManager;
import com.keep100days.principle.note.NoteModel;
import com.keep100days.principle.note.SaveNoteEvent;
import com.zhy.view.flowlayout.TagFlowLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class CopyObserverWindow {
    private static final String TAG = CopyObserverWindow.class.getName();

    private ClipboardManager manager;
    private ClipboardManager.OnPrimaryClipChangedListener clipChangedListener;
    private WindowManager windowManager;
    private ViewGroup urlLayout;

    private Animator slideAnimator;
    private Animator inAnimator;
    private Animator outAnimator;
    private Animator alphaAnimator;

    private String value;

    private GestureDetector detector;

    private float scrollProgress;
    private boolean mIsShown = false;

    private volatile static CopyObserverWindow mInstance;
    private boolean mInit = false;

    private static final long IN_ANIMATION_DURATION = 300L;
    private static final long OUT_ANIMATION_DURATION = 300L;
    private static final long SLIDE_ANIMATION_DURATION = 150L;
    private static final long VIEW_VISIBLE_DURATION = 3000L;
    private static final float TERMINATE_SLIDE_PROGRESS = 0.7f;

    private AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            remove();
        }
    };

    public static CopyObserverWindow getInstance(Context context) {
        if (mInstance == null) {
            synchronized (CopyObserverWindow.class) {
                if (mInstance == null) {
                    mInstance = new CopyObserverWindow(context);
                }
            }
        }
        return mInstance;
    }

    private Context context;

    private CopyObserverWindow(Context context) {
        this.context = context;
    }

    private void init() {
        this.windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        urlLayout = (ViewGroup) View.inflate(context, R.layout.layout_copy_observer_window, null);
        detector = new GestureDetector(context, new GestureDetector.OnGestureListener() {

            private float startX;
            private float dis;
            private int layoutWidth;

            @Override
            public boolean onDown(MotionEvent e) {

                startX = urlLayout.getX();
                dis = 0;
                layoutWidth = urlLayout.getMeasuredWidth();
                scrollProgress = 0f;
                return true;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                clearAllAnimator();
                dis += distanceX;
                float realDis = -(startX + dis);

                scrollProgress = realDis / layoutWidth;
                urlLayout.setX(realDis);
                urlLayout.setAlpha(1 - Math.abs(scrollProgress));
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

                slideToHide((int) velocityX);
                return true;
            }

        });

        urlLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (Math.abs(scrollProgress) > TERMINATE_SLIDE_PROGRESS) {
                        slideToHide((int) scrollProgress);
                    } else {
                        slide(urlLayout.getX(), 0f, SLIDE_ANIMATION_DURATION, null);
                        delayForHide();
                    }
                }

                return detector.onTouchEvent(event);
            }
        });

        urlLayout.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSave(null);
            }
        });

        urlLayout.findViewById(R.id.mark_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlLayout.findViewById(R.id.mark_layout).setVisibility(View.VISIBLE);
                final EditText editText = urlLayout.findViewById(R.id.edit_input);
                editText.requestFocus();
                SoftKeyUtil.showIME(editText.getContext(), editText);
                clearAllAnimator();


                ThreadPoolUtils.execute(new Runnable() {
                    @Override
                    public void run() {

                        final List<CommonModel> list = ModelDataManager.getAllOrderByTime(context, ModelType.TYPE_MARK);

                        if (context instanceof Activity && list != null && list.size() > 0) {
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    editText.setText(list.get(0).getContent());
                                    final TagFlowLayout flowLayout = urlLayout.findViewById(R.id.flow_layout);
                                    FlowLayoutAdapter adapter = new FlowLayoutAdapter(context, list);
                                    adapter.setOnChosenListener(new FlowLayoutAdapter.OnChosenListener() {
                                        @Override
                                        public void onChosen(String mark) {
                                            EditText editText = urlLayout.findViewById(R.id.edit_input);
                                            editText.setText(mark);
                                        }
                                    });
                                    flowLayout.setAdapter(adapter);
                                }
                            });
                        }
                    }
                });

            }
        });

        urlLayout.findViewById(R.id.iv_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = urlLayout.findViewById(R.id.edit_input);
                String mark = editText.getText().toString();
                editText.setText("");
                doSave(mark);
            }
        });

    }

    private void doSave(final String mark) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                NoteDataManager.insert(context, new NoteModel(value.trim(), mark));

                List<CommonModel> list = ModelDataManager.query(context, mark, ModelType.TYPE_MARK);
                if (list != null && list.size() >= 1) {
                    CommonModel model = list.get(0);
                    model.setUpdateTime(System.currentTimeMillis());
                    ModelDataManager.insert(context, model);
                } else {
                    ModelDataManager.insert(context, new CommonModel(mark, ModelType.TYPE_MARK));
                }

                if (context instanceof Activity) {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "保存成功", Toast.LENGTH_SHORT).show();
                            EventBus.getDefault().post(new SaveNoteEvent());
                        }
                    });
                }
            }
        });
        clearAllAnimator();
        remove();
    }

    public void register() {
        manager = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);

        if (manager != null) {
            // 防止重复添加listener
            if (clipChangedListener != null) {
                manager.removePrimaryClipChangedListener(clipChangedListener);
            }

            clipChangedListener = new ClipboardManager.OnPrimaryClipChangedListener() {
                @Override
                public void onPrimaryClipChanged() {

                    if (manager.hasPrimaryClip() && manager.getPrimaryClip() != null
                            && manager.getPrimaryClip().getItemCount() > 0) {

                        CharSequence addedText = manager.getPrimaryClip().getItemAt(0).getText();
                        if (addedText != null) {
                            show(addedText.toString());
                        }
                    }
                }
            };

            manager.addPrimaryClipChangedListener(clipChangedListener);
        }

    }

    public void unregister() {
        if (manager != null) {
            manager.removePrimaryClipChangedListener(clipChangedListener);
        }
        remove();
    }

    public void show(String value) {
        if (!mInit) {
            mInit = true;
            init();
        }
        this.value = value.trim();
        clearAllAnimator();
        remove();
        urlLayout.setVisibility(View.VISIBLE);
        urlLayout.setAlpha(1f);
        urlLayout.setX(0f);
        TextView tvContent = urlLayout.findViewById(R.id.url_text);
        tvContent.setText(value);

        windowManager.addView(urlLayout, getDefaultParams());
        mIsShown = true;
        inAnimator = ObjectAnimator.ofFloat(urlLayout, View.ALPHA, 0f, 1f);
        inAnimator.setDuration(IN_ANIMATION_DURATION);
        inAnimator.setInterpolator(new DecelerateInterpolator(1.5f));
        inAnimator.start();
        delayForHide();
    }

    private void delayForHide() {
        outAnimator = ObjectAnimator.ofFloat(urlLayout, View.ALPHA, 1f, 0f);
        outAnimator.setDuration(OUT_ANIMATION_DURATION);
        outAnimator.setStartDelay(VIEW_VISIBLE_DURATION);
        outAnimator.setInterpolator(new DecelerateInterpolator(1.5f));
        outAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                remove();
            }
        });
        outAnimator.start();
    }

    private void slide(float startX, float endX, long duration, AnimatorListenerAdapter animatorListenerAdapter) {
        clearAllAnimator();
        alphaAnimator = ObjectAnimator.ofFloat(urlLayout, View.ALPHA, urlLayout.getAlpha(), 1f);
        slideAnimator = ObjectAnimator.ofFloat(urlLayout, View.X, startX, endX);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(alphaAnimator)
                .with(slideAnimator);

        animatorSet.setDuration(duration);
        if (animatorListenerAdapter != null) {
            animatorSet.addListener(animatorListenerAdapter);
        }
        animatorSet.start();
    }

    public void remove() {
        if (mIsShown) {
            urlLayout.setVisibility(View.INVISIBLE);
            urlLayout.findViewById(R.id.mark_layout).setVisibility(View.GONE);
            windowManager.removeView(urlLayout);
            mIsShown = false;
        }
    }

    private void slideToHide(int direction) {
        int endX;

        if (direction > 0) {
            endX = DensityUtil.getScreenWidth(context);
        } else {
            endX = -DensityUtil.getScreenWidth(context);
        }
        slide(urlLayout.getX(), endX, SLIDE_ANIMATION_DURATION, animatorListenerAdapter);
    }

    private void clearAllAnimator() {
        clearAnimator(inAnimator);
        clearAnimator(slideAnimator);
        clearAnimator(outAnimator);
        clearAnimator(alphaAnimator);
    }

    private void clearAnimator(Animator animator) {
        if (animator != null) {
            animator.removeAllListeners();
            animator.cancel();
        }
    }

    public int getImRapidWindowFlag() {
        int flag = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            flag = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        }
        return flag;
    }

    private WindowManager.LayoutParams getDefaultParams() {
        int flag = getImRapidWindowFlag();

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                flag,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
//                        | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
                        | WindowManager.LayoutParams.FLAG_FULLSCREEN
                        | WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
                        | WindowManager.LayoutParams.FLAG_SPLIT_TOUCH,
                PixelFormat.TRANSLUCENT);
        params.format = PixelFormat.RGBA_8888;
        params.gravity = Gravity.TOP;
        params.setTitle("COPY_OBSERVER_WINDOW");
        params.packageName = context.getPackageName();
        params.y = DensityUtil.dp2px(context, 40);
        return params;
    }
}