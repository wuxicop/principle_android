package com.keep100days.principle.note.window;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common_model.CommonModel;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;

import java.util.List;

/**
 * @author a
 */
public class FlowLayoutAdapter extends TagAdapter<CommonModel> {

    private Context context;
    private List<CommonModel> datas;

    public FlowLayoutAdapter(Context context, List<CommonModel> datas) {
        super(datas);
        this.context = context;
        this.datas = datas;
    }

    @Override
    public View getView(FlowLayout parent, int position, CommonModel model) {
        TextView tv = (TextView) LayoutInflater.from(context).inflate(R.layout.mark_item,
                parent, false);
        tv.setText(model.getContent());
        return tv;
    }

    @Override
    public void onSelected(int position, View view) {
        super.onSelected(position, view);
        String mark = datas.get(position).getContent();
        if (mListener != null) {
            mListener.onChosen(mark);
        }
    }

    private OnChosenListener mListener;

    public void setOnChosenListener(OnChosenListener listener) {
        mListener = listener;
    }

    public interface OnChosenListener {
        void onChosen(String mark);
    }
}
