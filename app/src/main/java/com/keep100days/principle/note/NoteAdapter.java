package com.keep100days.principle.note;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.ui.ContentDetailActivity;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.CenterPopUpWindow;
import com.keep100days.principle.common.widget.dslv.DragSortListView;

import java.util.List;

import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_UPDATE;

public class NoteAdapter extends BaseAdapter implements DragSortListView.DragSortListener {

    private Activity mContext;
    private List<NoteModel> mList;
    private NoteAdapter.ActionListener mActionListener;

    public NoteAdapter(Activity context, List<NoteModel> list) {
        mContext = context;
        mList = list;
    }

    public void setActionListener(NoteAdapter.ActionListener listener) {
        this.mActionListener = listener;
    }

    class NoteViewHolder {
        TextView tvContent;
        TextView tvMark;
        View markLine;
        View contentLayout;

        NoteViewHolder(@NonNull View itemView) {
            tvContent = itemView.findViewById(R.id.tv_note_content);
            tvMark = itemView.findViewById(R.id.tv_mark);
            markLine = itemView.findViewById(R.id.mark_line);
            contentLayout = itemView.findViewById(R.id.content_layout);
        }
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public NoteModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NoteViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.note_item, parent, false);
            holder = new NoteViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (NoteViewHolder) convertView.getTag();
        }

        final NoteModel model = mList.get(position);
        int index = position + 1;
        String content = model.getContent();
        content = content.replace("\n", "");
        final String result = mContext.getString(R.string.formatted_item_content, index, content);
        holder.tvContent.setText(result);

        if (TextUtils.isEmpty(model.getMark())) {
            holder.markLine.setVisibility(View.GONE);
            holder.tvMark.setVisibility(View.GONE);
        } else {
            holder.markLine.setVisibility(View.VISIBLE);
            holder.tvMark.setVisibility(View.VISIBLE);
            holder.tvMark.setText(model.getMark());
        }

        final String finalContent = content;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentDetailActivity.start(mContext, finalContent, model.getMark());
            }
        });

        final View itemView = convertView;

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopUpWindow(model, itemView);
                return true;
            }
        });

        return convertView;
    }

    public void showPopUpWindow(NoteModel model, View view) {
        CenterPopUpWindow<NoteModel> window = new CenterPopUpWindow<NoteModel>(mContext, model, view, ACTION_UPDATE) {
            @Override
            public void doDelete(NoteModel model) {
                if (mActionListener != null) {
                    mActionListener.doDelete(model);
                }
            }

            @Override
            public void doUpdate(NoteModel model) {
                if (mActionListener != null) {
                    mActionListener.doUpdate(model);
                }
            }
        };
        window.show();
    }

    @Override
    public void drag(int from, int to) {

    }

    @Override
    public void drop(final int from, final int to) {
        if (from != to) {
            NoteModel model = mList.remove(from);
            mList.add(to, model);

            ThreadPoolUtils.execute(new Runnable() {
                @Override
                public void run() {
                    NoteDataManager.swapIndex(mContext, mList.get(from), mList.get(to));
                }
            });

            notifyDataSetChanged();
        }
    }

    public void updateData(List<NoteModel> list){
        mList.clear();
        mList.addAll(list);
    }

    @Override
    public void remove(int which) {
        mList.remove(which);
        notifyDataSetChanged();
    }

    public interface ActionListener {

        void doDelete(NoteModel model);

        void doUpdate(NoteModel model);
    }
}
