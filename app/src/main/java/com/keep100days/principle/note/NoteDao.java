package com.keep100days.principle.note;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * @author liu
 */

@Dao
public interface NoteDao {
    @Query("select * FROM note_table order by `index` asc")
    List<NoteModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertRecord(NoteModel entity);

    @Delete()
    int deleteRecord(NoteModel entity);

    @Update()
    int updateRecord(NoteModel entity);

    @Query("update note_table set `index`=:index where id =:id")
    void updateRecordIndexById(long id, long index);
}
