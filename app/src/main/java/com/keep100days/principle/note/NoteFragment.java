package com.keep100days.principle.note;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.BaseFragment;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common.widget.dslv.ShadowFloatViewManager;
import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;
import com.keep100days.principle.common_model.ModelType;
import com.keep100days.principle.eventpool.EventPoolActivity;
import com.keep100days.principle.note.notedirect.NoteDirectActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author a
 */

public class NoteFragment extends BaseFragment implements NoteAdapter.ActionListener {

    public static final String TAG = "NoteFragment";

    private NoteAdapter mAdapter;

    private NotePresenter mPresenter;

    private TextView mTvDirectNum;

    public static NoteFragment newInstance() {
        return new NoteFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        mPresenter = new NotePresenter();

        mAdapter = new NoteAdapter(getActivity(), new ArrayList<NoteModel>());
        mAdapter.setActionListener(this);
    }

    @Override
    public void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(getActivity(), content, null);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_note, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DragSortListView listView = view.findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(getActivity()));
        listView.addFooterView(new View(getActivity()));
        listView.setAdapter(mAdapter);

        ShadowFloatViewManager controller = new ShadowFloatViewManager(listView);
        listView.setFloatViewManager(controller);

        mTvDirectNum = view.findViewById(R.id.tv_note_direct_num);
        view.findViewById(R.id.layout_note_direct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoteDirectActivity.start(getActivity());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();

        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {

                List<CommonModel> list = ModelDataManager.getAll(getActivity(), ModelType.TYPE_NOTE_DIRECT);
                final int num = list == null ? 0 : list.size();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mTvDirectNum.setText(String.valueOf(num));
                    }
                });
            }
        });
    }

    @Override
    public void doDelete(final NoteModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doDelete(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void doUpdate(final NoteModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doUpdate(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMoonEvent(SaveNoteEvent event) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
