package com.keep100days.principle.note;

import android.app.Activity;
import android.content.Context;

import java.util.List;

/**
 * @author a
 */

public class NotePresenter {

    public void doRefresh(Activity activity, final NoteAdapter adapter) {
        final List<NoteModel> list = NoteDataManager.getAll(activity);
        if (list != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(list);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void doAddData(Activity activity, String content, String mark) {
        NoteModel model = new NoteModel(content, mark);
        NoteDataManager.insert(activity, model);
    }

    public void doDelete(Activity activity, NoteModel model) {
        NoteDataManager.delete(activity, model);
    }

    public void doUpdate(Context context, NoteModel model) {
        NoteDataManager.update(context, model);
    }
}
