package com.keep100days.principle.note;

import android.content.Context;

import com.keep100days.principle.db.AppDatabaseHelper;

import java.util.List;

public class NoteDataManager {


    public static List<NoteModel> getAll(Context context) {
        try {
            return AppDatabaseHelper.getInstance(context).noteDao().getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void insert(Context context, NoteModel model) {
        try {
            long id = AppDatabaseHelper.getInstance(context).noteDao().insertRecord(model);
            updateIndexById(context, id, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void delete(Context context, NoteModel model) {
        try {
            AppDatabaseHelper.getInstance(context).noteDao().deleteRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, NoteModel model) {
        try {
            AppDatabaseHelper.getInstance(context).noteDao().updateRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateIndexById(Context context, long id, long index) {
        try {
            AppDatabaseHelper.getInstance(context).noteDao().updateRecordIndexById(id, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void swapIndex(Context context, NoteModel left, NoteModel right) {
        long leftIndex = left.getIndex();

        updateIndexById(context, left.getId(), right.getIndex());
        updateIndexById(context, right.getId(), leftIndex);
    }
}
