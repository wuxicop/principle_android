package com.keep100days.principle.todo;

import android.app.Activity;
import android.content.Context;

import com.keep100days.principle.common.util.SpUtil;
import com.keep100days.principle.common.util.TimeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author liu
 */

public class TodoPresenter {

    private void sortItems(List<TodoModel> list) {
        Collections.sort(list, new Comparator<TodoModel>() {
            @Override
            public int compare(TodoModel left, TodoModel right) {
                return doCompare(left, right);
            }
        });
    }

    /**
     * 标星的，靠前
     * 时间越新，越靠前(调过顺序，以顺序为准)
     * 未完成的，靠前
     */
    private int doCompare(TodoModel left, TodoModel right) {
        int result = getCompareWeight(left) - getCompareWeight(right);
        if (result == 0) {
            result = getStarCompareWeight(left) - getStarCompareWeight(right);
            if (result == 0) {
                long offset = left.getIndex() - right.getIndex();
                result = offset < 0 ? 1 : -1;
            }
        }
        return result;
    }

    private int getStarCompareWeight(TodoModel model) {
        if (model == null) {
            return Integer.MAX_VALUE;
        }
        return model.isStar() ? 0 : 1;
    }

    private int getCompareWeight(TodoModel model) {
        if (model == null) {
            return Integer.MAX_VALUE;
        }
        return model.isDone() ? 1 : 0;
    }

    public void doDelete(Context context, TodoModel model) {
        TodoDataManager.delete(context, model);
    }

    public void doUpdate(Context context, TodoModel model) {
        TodoDataManager.update(context, model);
    }

    public void doAddData(Context context, String content) {
        TodoModel model = new TodoModel(content);
        TodoDataManager.insert(context, model);
    }

    public void doRefresh(Activity activity, final TodoAdapter adapter) {

        boolean onlyNotDone = SpUtil.isOnlyShowNotDoneTodo(activity);
        boolean onlyToday = SpUtil.isOnlyShowTodayTodo(activity);

        List<TodoModel> list = TodoDataManager.getAll(activity);
        if (list != null) {
            if (onlyNotDone) {
                list = getOnlyNotDoneItems(list);
            }

            if (onlyToday) {
                list = getOnlyTodayItems(list);
            }

            sortItems(list);

            final List<TodoModel> finalList = list;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.updateData(finalList);
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

    private List<TodoModel> getOnlyTodayItems(List<TodoModel> list) {
        List<TodoModel> result = new ArrayList<>();
        for (TodoModel model : list) {
            if (TimeUtil.isToday(model.getCreateTime())) {
                result.add(model);
            }
        }
        return result;
    }

    private List<TodoModel> getOnlyNotDoneItems(List<TodoModel> list) {
        List<TodoModel> result = new ArrayList<>();
        for (TodoModel model : list) {
            if (!model.isDone()) {
                result.add(model);
            }
        }
        return result;
    }
}
