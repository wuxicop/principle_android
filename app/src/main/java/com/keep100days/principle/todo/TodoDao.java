package com.keep100days.principle.todo;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TodoDao {

    @Query("select * FROM todo_table order by `index` asc")
    List<TodoModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertRecord(TodoModel entity);

    @Delete()
    int deleteRecord(TodoModel entity);

    @Update()
    int updateRecord(TodoModel entity);

    @Query("update todo_table set `index`=:index where id =:id")
    int updateRecordIndexById(long id, long index);
}
