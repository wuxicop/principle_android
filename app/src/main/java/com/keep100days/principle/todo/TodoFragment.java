package com.keep100days.principle.todo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.BaseFragment;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.widget.dslv.DragSortListView;
import com.keep100days.principle.common_model.CommonModel;
import com.keep100days.principle.common_model.ModelDataManager;
import com.keep100days.principle.common_model.ModelType;
import com.keep100days.principle.eventpool.EventPoolActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liu
 */
public class TodoFragment extends BaseFragment implements TodoAdapter.ActionListener {

    public static final String TAG = "TodoFragment";

    private TodoAdapter mAdapter;

    private TextView tvEventPoolNum;

    private TodoPresenter mPresenter;

    public static TodoFragment newInstance() {
        return new TodoFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new TodoPresenter();

        mAdapter = new TodoAdapter(getActivity(), new ArrayList<TodoModel>());
        mAdapter.setActionListener(this);
    }

    @Override
    public void refresh() {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void addData(final String content) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doAddData(getActivity(), content);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_todo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DragSortListView listView = view.findViewById(R.id.recycler_view);
        listView.addHeaderView(new View(getActivity()));
        listView.addFooterView(new View(getActivity()));
        listView.setAdapter(mAdapter);

        tvEventPoolNum = view.findViewById(R.id.tv_event_pool_num);

        View eventPoolView = view.findViewById(R.id.layout_event_pool);

        eventPoolView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventPoolActivity.start(getActivity());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();

        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {

                List<CommonModel> list = ModelDataManager.getAll(getActivity(), ModelType.TYPE_EVENT_POOL);
                final int num = list == null ? 0 : list.size();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvEventPoolNum.setText(String.valueOf(num));
                    }
                });
            }
        });
    }

    @Override
    public void doDelete(final TodoModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doDelete(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }

    @Override
    public void doUpdate(final TodoModel model) {
        ThreadPoolUtils.execute(new Runnable() {
            @Override
            public void run() {
                mPresenter.doUpdate(getActivity(), model);
                mPresenter.doRefresh(getActivity(), mAdapter);
            }
        });
    }
}
