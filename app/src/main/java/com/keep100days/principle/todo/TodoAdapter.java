package com.keep100days.principle.todo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.keep100days.principle.R;
import com.keep100days.principle.common.ui.ContentDetailActivity;
import com.keep100days.principle.common.util.ThreadPoolUtils;
import com.keep100days.principle.common.util.TimeUtil;
import com.keep100days.principle.common.widget.CenterPopUpWindow;
import com.keep100days.principle.common.widget.dslv.DragSortListView;

import java.util.List;

import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_TO_DONE;
import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_TO_NOT_DONE;
import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_TO_NOT_STAR;
import static com.keep100days.principle.common.widget.CenterPopUpWindow.ACTION_TO_STAR;

/**
 * @author liu
 */

public class TodoAdapter extends BaseAdapter implements DragSortListView.DragSortListener {

    private Activity mContext;
    private List<TodoModel> mList;
    private ActionListener mActionListener;
    private static final int ACTION_STAR_POS = 2;


    public TodoAdapter(Activity context, List<TodoModel> list) {
        mContext = context;
        mList = list;
    }

    public void setActionListener(ActionListener listener) {
        this.mActionListener = listener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public TodoModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TodoViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.todo_item, parent, false);
            holder = new TodoViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (TodoViewHolder) convertView.getTag();
        }

        final TodoModel model = mList.get(position);
        int index = position + 1;
        final String content = model.getContent();
        final String result = mContext.getString(R.string.formatted_item_content, index, content);
        holder.tvContent.setText(result);

        long time = model.getCreateTime();
        holder.tvCreateTime.setText(TimeUtil.getFormattedTime(time));

        if (model.isDone() || TimeUtil.isToday(time)) {
            holder.tvPassedDays.setVisibility(View.GONE);
        } else {
            holder.tvPassedDays.setVisibility(View.VISIBLE);
        }

        if (model.isDone()) {
            holder.tvCreateTime.setTextColor(mContext.getResources().getColor(R.color.color_done_work));
            holder.tvCreateTime.setAlpha(0.4f);
            holder.tvContent.setAlpha(0.4f);
            holder.dragView.setVisibility(View.GONE);
        } else {
            if (!TimeUtil.isToday(time)) {
                holder.tvCreateTime.setTextColor(mContext.getResources().getColor(R.color.color_undone_work));
                holder.tvCreateTime.setAlpha(1.0f);
                int days = TimeUtil.getPassedDaysNum(time);
                holder.tvPassedDays.setText(mContext.getString(R.string.passed_days, days));
            } else {
                holder.tvCreateTime.setTextColor(mContext.getResources().getColor(R.color.color_done_work));
                holder.tvCreateTime.setAlpha(0.4f);
            }
            holder.tvContent.setAlpha(1.0f);
        }

        if (model.isStar()) {
            holder.tvContent.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        } else {
            holder.tvContent.setTextColor(mContext.getResources().getColor(R.color.default_text_color));
        }

        //TODO 排序功能暂时隐藏
        holder.dragView.setVisibility(View.GONE);

        holder.tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentDetailActivity.start(mContext, content);
            }
        });

        final View finalView = convertView;
        holder.tvContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopUpWindow(model, finalView);
                return true;
            }
        });
        return convertView;
    }

    public void showPopUpWindow(TodoModel model, View view) {

        String updateAction = model.isDone() ? ACTION_TO_NOT_DONE : ACTION_TO_DONE;

        String starAction = model.isStar() ? ACTION_TO_NOT_STAR : ACTION_TO_STAR;

        CenterPopUpWindow<TodoModel> window = new CenterPopUpWindow<TodoModel>(mContext, model, view, updateAction, starAction) {
            @Override
            public void doDelete(TodoModel model) {
                if (mActionListener != null) {
                    mActionListener.doDelete(model);
                }
            }

            @Override
            public void doUpdate(TodoModel model) {
                if (mActionListener != null) {
                    boolean isDone = model.isDone();
                    model.setDone(!isDone);
                    mActionListener.doUpdate(model);
                }
            }

            @Override
            public void doCustomActionClick(TodoModel model, int position) {
                super.doCustomActionClick(model, position);
                if (position == ACTION_STAR_POS && mActionListener != null) {
                    boolean isStar = model.isStar();
                    model.setStar(!isStar);
                    mActionListener.doUpdate(model);
                }
            }
        };
        window.show();
    }

    @Override
    public void drag(int from, int to) {

    }

    @Override
    public void drop(final int from, final int to) {
        if (from != to) {
            TodoModel model = mList.remove(from);
            mList.add(to, model);

            ThreadPoolUtils.execute(new Runnable() {
                @Override
                public void run() {
                    TodoDataManager.swapIndex(mContext, mList.get(from), mList.get(to));
                }
            });

            notifyDataSetChanged();
        }
    }

    @Override
    public void remove(int which) {
        mList.remove(which);
        notifyDataSetChanged();
    }

    public void updateData(List<TodoModel> list) {
        mList.clear();
        mList.addAll(list);
    }

    class TodoViewHolder {
        TextView tvContent;
        TextView tvCreateTime;
        TextView tvPassedDays;
        View dragView;

        TodoViewHolder(View itemView) {
            tvContent = itemView.findViewById(R.id.tv_todo_content);
            tvCreateTime = itemView.findViewById(R.id.tv_create_time);
            tvPassedDays = itemView.findViewById(R.id.tv_passed_days);
            dragView = itemView.findViewById(R.id.drag_handle_view);
        }
    }

    public interface ActionListener {

        void doDelete(TodoModel model);

        void doUpdate(TodoModel model);
    }
}
