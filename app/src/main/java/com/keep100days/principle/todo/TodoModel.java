package com.keep100days.principle.todo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * @author liu
 */

@Entity(tableName = "todo_table")
public class TodoModel {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String content;

    @ColumnInfo(name = "create_time")
    private long createTime;

    @ColumnInfo(name = "is_done")
    private boolean isDone;

    private long index;

    private boolean isStar;

    public TodoModel(String content){
        setContent(content);
        setDone(false);
        setCreateTime(System.currentTimeMillis());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public boolean isStar() {
        return isStar;
    }

    public void setStar(boolean star) {
        isStar = star;
    }

    public static String getAlterSQL() {
        return "ALTER TABLE todo_table ADD COLUMN isStar INTEGER DEFAULT 0 NOT NULL";
    }
}
