package com.keep100days.principle.todo;

import android.content.Context;

import com.keep100days.principle.db.AppDatabaseHelper;

import java.util.List;

/**
 * @author liu
 */

public class TodoDataManager {
    
    public static List<TodoModel> getAll(Context context) {
        try {
            return AppDatabaseHelper.getInstance(context).todoDao().getAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void insert(Context context, TodoModel model){
        try {
            long id = AppDatabaseHelper.getInstance(context).todoDao().insertRecord(model);
            updateIndexById(context, id, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void delete(Context context, TodoModel model) {
        try {
            AppDatabaseHelper.getInstance(context).todoDao().deleteRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateIndexById(Context context, long id, long index) {
        try {
           AppDatabaseHelper.getInstance(context).todoDao().updateRecordIndexById(id, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(Context context, TodoModel model) {
        try {
            AppDatabaseHelper.getInstance(context).todoDao().updateRecord(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void swapIndex(Context context, TodoModel left, TodoModel right) {
        long leftIndex = left.getIndex();

        updateIndexById(context, left.getId(), right.getIndex());
        updateIndexById(context, right.getId(), leftIndex);
    }
}
